var master = function(urls) {
	var 
	adverts = urls.adverts,
	adverts_link = urls.adverts_link,
	home = urls.home,
	advert_ajax = urls.advert_ajax,
	delete_property_image = urls.delete_property_image,
	change_status = urls.change_status,
	send_email = urls.send_email,
	png_advert_count = 0, jpg_advert_count = 0;

	(function() {
		console.log('self invoking method');
		$('#description').jqte();
	})();

	/*contact us tab*/
	$('#myTab a').click(function (e) {
		var href = $(this).attr('href');
		console.log(href);
	  	e.preventDefault();
	  	$(this).tab('show');
	});

	/*use to enable/disable adverts */
	$('.advert-btn').on("click", function(){
		var 
		this_btn = $(this),
		btn_text = this_btn.text(),
		id = this_btn.attr('id'),
		path = advert_ajax;
		$.ajax({
			type: "POST",
			url:path,
			data: {
				"id": id,
			},
			beforeSend: function(){
				this_btn.text('Loading...');
			},
			success: function(data) {
				this_btn.text(data);
			},
			error: function(xhr, text, error) {
				this_btn.text(btn_text);
				console.log(xhr+" >>> "+text+" >>> "+error);
			},
		});
	});

	/*Deleting a property image via ajax*/
	$('.del-prop-img').on('click', function(){
		var 
		btn = $(this),
		img_name = btn.attr('img-name'),
		property_id = btn.attr('property-id'),
		url = delete_property_image;

		$.ajax({
			type: "POST",
			url: url,
			data: {
				img_name: img_name,
				property_id: property_id,
			},
			beforeSend: function(){
				console.log('Updating...');
			},
			success: function(data) {
				console.log(data);
			},
			error: function(xhr, text, error) {
				console.log(xhr+" >>> "+text+" >>> "+error);
			},
		});

		location.reload(true);
	});

	/*home page carousel*/
	$('.carousel').carousel({
	  interval: 4000
	});

	/*anonymous function for refreshing of the advert images*/
	var 
	dir = adverts,
	image_jpg_array = [], 
	image_png_array = [], 
	json_all_images;
	(function(){
		$.ajax({
			dataType: 'json',
		    url: adverts_link,
		    success: function (data) {
		    	$.each(data, function(key, value) {
		    		splitter = key.split('.');
		    		if (splitter[splitter.length -1] === "jpg") {
		    			image_jpg_array.push(key);
		    		} else if (splitter[splitter.length -1] === "png") {
		    			image_png_array.push(key);
		    		}
		    	});

				json_all_images = data;
		    }
		}).done(function(data) {
			initial_load();
      	});

	})();

	/*
	triggered when property submit button is clicked. used to check if it's an edit or an addition.
	the user should upload atlease 1 image for the property. If it's an edit, the user must not 
	upload an image. It's also important to note that only images with the correct dimensions will
	uploaded.
	*/
	$("#property-submit-btn").on("click", function(){
		if (property_action === 'property should be ADDED')  {
			if (image_upload_count < 1){
				$("#no-images-error-msg").show();
				return false;
			}
		} else if (property_action === 'property should be EDITED'){
			if (total_images_incorrect_dimensions !== 0) {
				dimension_error_message();
			}
			return true;
		}
		if (total_images_incorrect_dimensions > 0) {
			dimension_error_message();
		}
		return true;
	});

	function dimension_error_message(){
		$("#no-images-error-msg")
			.empty()
			.append("Some of the images uploaded have incorrect dimensions; you either have less than 725 pixels in width and 295 pixels in height or more than 2000 pixels i5 width and 1500 pixles in height")
			.show();
		return false;
	}

	$('#broker-sumbit').on("click", function(){
		if (total_images_incorrect_dimensions > 0) {
			$("#invalid-dimensions-error").show();
			return false;
		}
		return true;
	});

	$("input[name='logo_path']").change(function (e) {
	    var file = this.files[0];

	    var reader = new FileReader();
	    var image  = new Image();

	    reader.readAsDataURL(file);  
	    reader.onload = function(_file) {
	        image.src    = _file.target.result;              // url.createObjectURL(file);
	        image.onload = function() {
	            var w = this.width,
	                h = this.height,
	                t = file.type,                           // ext only: // file.type.split('/')[1],
	                n = file.name,
	                s = ~~(file.size/1024) +'KB';
	                if (w > 140 || h > 140) {
	                	total_images_incorrect_dimensions ++;
	                } else {
	                	if (total_images_incorrect_dimensions !== 0){
	                		total_images_incorrect_dimensions --;
	                	}
	                }
	                //'image source::'+ this.src +
	            console.log('" width:'+w+' height:'+h+' size:'+s+' type:'+t+' file-name:'+n);
	            image_upload_count++;
	        };
	        image.onerror= function() {
	            console.log('Invalid file type: '+ file.type);
	        };      
	    };
	});

	/*START::::::::::::::::UPLOAD SIZE SECTION*/
	var property_action = "{{ (isset($edit_property)) ? 'property should be EDITED' : 'property should be ADDED'  }}";

	$("#no-images-error-msg").hide();
	$("#invalid-dimensions-error").hide();
	var total_images_incorrect_dimensions = 0, image_upload_count = 0;
	$("input[name='image[]']").change(function (e) {
	    if(this.disabled) return alert('File upload not supported!');
	    var F = this.files;
	    if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] );
	});

	function readImage(file) {
	    var reader = new FileReader();
	    var image  = new Image();

	    reader.readAsDataURL(file);  
	    reader.onload = function(_file) {
	        image.src    = _file.target.result;              // url.createObjectURL(file);
	        image.onload = function() {
	            var w = this.width,
	                h = this.height,
	                t = file.type,                           // ext only: // file.type.split('/')[1],
	                n = file.name,
	                s = ~~(file.size/1024) +'KB';
	                if ((w < 640 && h < 440) || (w > 2000 && h > 1500)) {
	                	total_images_incorrect_dimensions ++;
	                } else {
	                	if (total_images_incorrect_dimensions !== 0){
	                		total_images_incorrect_dimensions --;
	                	}
	                }
	                //'image source::'+ this.src +
	            console.log('" width:'+w+' height:'+h+' size:'+s+' type:'+t+' file-name:'+n);
	            image_upload_count++;
	        };
	        image.onerror= function() {
	            console.log('Invalid file type: '+ file.type);
	        };      
	    };

	}
	/*STOP::::::::::::::::UPLOAD SIZE SECTION*/

	/*START::::::::::::ADVERT SECTION*/
	function initial_load(){
		var
		img_png = $("<img>", { src: dir+image_png_array[png_advert_count], class:"img-advert-png" }),
		img_jpg	= $("<img>", { src: dir+image_jpg_array[jpg_advert_count], class:"img-advert-jpg" }),
		png_link = getLink(image_png_array[0]),
		jpg_link = getLink(image_jpg_array[0]);

	    $(".png").append($("<a>", { href:png_link }).append(img_png));
	    $(".jpg").append($("<a>", { href:jpg_link }).append(img_jpg));
	}

	function getLink(image_name) {
		var img_link;
		$.each(json_all_images, function(name, link){
			if (name === image_name) {
				img_link = (link.length <= 0) ? home : link;
				return false;
			}
		});
		return img_link;
	}


	function append_advert(){
		(png_advert_count === image_png_array.length-1 ) ?  png_advert_count = 0 : png_advert_count++;
		(jpg_advert_count === image_jpg_array.length-1 ) ?  jpg_advert_count = 0 : jpg_advert_count++;

		var 
		img_png = $("<img>", { src: dir+image_png_array[png_advert_count], class:"img-advert-png" }),
		img_jpg = $("<img>", { src: dir+image_jpg_array[jpg_advert_count], class:"img-advert-jpg" }),
		png_link = getLink(image_png_array[png_advert_count]),
		jpg_link = getLink(image_jpg_array[jpg_advert_count]);

		($(".img-advert-png").length === 0) ? $(".png").append(img_png) : $(".png").empty().append($("<a>", { href:png_link }).append(img_png));
		($(".img-advert-jpg").length === 0) ? $(".jpg").append(img_jpg) : $(".jpg").empty().append($("<a>", { href:jpg_link }).append(img_jpg));
	}

	//advert images updates after 5 seconds
	setInterval(append_advert,8000);
	/*STOP::::::::::::ADVERT SECTION*/

	$('#payment a').click(function (e) {
	 	e.preventDefault();
	  	$(this).tab('show');
	});

	$("#submit-transaction-code").on("click", function(){
		if ($("#transaction-code").val() === "") {
			return false;
		}
		return true;
	});

	$(".change-status").on("click", function(event){
		console.log("change status has been clicked");
		var 
		btn = $(this), 
		user_id = btn.attr('data-user-id'),
		refresh = btn.attr('data-refresh-page'),
		add_not_paid = btn.attr('data-add-not-paid');
		
		$.ajax({
			type: "POST",
			url: change_status,
			success: function(data) {
				console.log("data >>>"+data);
				if (data == 0){
					btn.text("False");
					btn.attr('class', 'btn btn-danger change-status')
				} else if (data == 1) {
					btn.text("True");
					btn.attr('class', 'btn btn-success change-status');
				}
			},
			data: {
				"user-id": user_id,
				"add_not_paid": add_not_paid,
			}
		});
		/*if (refresh !== "") {
			location.reload(true);
		}*/
		event.preventDefault();
	});

	$('.pending-approval').on('click', function(){
		$("#invoice-id").val($(this).attr("data-invoice-id"));
		$("#owner").val($(this).attr("data-owner"));
	});

	$("#approval-modal-submit").on('click', function(){
		if ($("#paid-amount").val() === "") {
			return false;
		}
		$("#invoice-id").removeAttr('disabled');
		$("#owner").removeAttr('disabled');
		return true;
	});

	$('#send-email').on("click", function(){
		/*TODO:add a loading status thingi*/
		$.ajax({
		    type: "POST",
		    dataType: "html",
		    url: send_email,
		    beforeSend: function(){
		        console.log("before send");
		    	var return_value = true, error_stack = [], error_msg, close_btn;
		    	if ($("#email").val() === "") {
		    		if ($("#email-error-msg").length === 0) {
			    		error_msg = $("<div>", {text:"enter a valid email", id:"email-error-msg", class:"alert alert-error"});
			    		close_btn = $("<button>", { class:"close", text:"×", "data-dismiss":"alert", });
			    		error_msg.prepend(close_btn)
		    			$("#email").after(error_msg);
		    		}
		    		return_value = false;
		    	} else {
		    		$("#email-error-msg").remove();
		    	}
		    	if ($("#propective-client-name").val() === "") {
		    		if ($("#name-error-msg").length === 0){
			    		error_msg = $("<div>", {text:"enter a valid name", id:"name-error-msg", class:"alert alert-error"});
			    		close_btn = $("<button>", { class:"close", text:"×", "data-dismiss":"alert", });
			    		error_msg.prepend(close_btn)
			    		$("#propective-client-name").after(error_msg);
		    		}
		    		return_value = false;
		    	} else {
		    		$("#name-error-msg").remove();
		    	}
		    	if ($("#phone-number").val() === "") {
		    		if ($("#phone-num-error-msg").length === 0) {
			    		error_msg = $("<div>", {text:"enter a valid phone number", id:"phone-num-error-msg", class:"alert alert-error"});
			    		close_btn = $("<button>", { class:"close", text:"×", "data-dismiss":"alert", });
			    		error_msg.prepend(close_btn)
			    		$("#phone-number").after(error_msg);
		    		}
		    		return_value = false;
		    	} else {
		    		$("#phone-num-error-msg").remove();
		    	}
		    	if ($("#email-message").val() === "") {
		    		if ($("#message-error-msg").length === 0) {
			    		error_msg = $("<div>", {text:"enter valid message", id:"message-error-msg", class:"alert alert-error"});
			    		close_btn = $("<button>", { class:"close", text:"×", "data-dismiss":"alert", });
			    		error_msg.prepend(close_btn)
			    		$("#email-message").after(error_msg);
		    		}
		    		return_value = false;
		    	} else {
		    		$("#message-error-msg").remove();
		    	}
		    	return return_value;
		    },
		    success:function(){
		    	$("#contact-owner-modal .modal-body").empty().append("You email was sent!! ");
		    },
		    data: { 
		      reciever: "{{ (isset($property->owner)) ? $property->owner : ''  }}", 
		      sender: $("#email").val(),
		      "propective-client-name": $("#propective-client-name").val(),
		      phone: $("#phone-number").val(),  
		      message: $("#email-message").val(), 
		   }
		}).done(function(data) {
		    console.log("finished");
		});
	});
};