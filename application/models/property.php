<?php 
class Property extends Eloquent {
	public static $timestamps = true ;
	public static $table = 'property';

	public function property_data()
	{
		return array(
						'residential' => array('house/villa', 'town house', 'apartment'),
						'land' => array('commercial', 'residential', 'agricultural', 'industrial'),
						'commercial and other' => array('commercial space', 'office', 'builiding/warehouse', 'existing business'),
						'holiday' => array('villa/house', 'apartment', 'town house', 'boutique accomodation', 'room') 
					);
	}
}
?>