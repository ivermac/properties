<?php

class Home_Controller extends Base_Controller {

	/**
	* this is the index action with login session details and a footer
	*/
	public function action_index()
	{
		$view = View::make('home.index');
		$login_error_msg = Session::get('login_error_msg');
		$login_success_msg = Session::get('login_success_msg');
		if (isset($login_error_msg)) {
			$view->login_error_msg = Session::get('login_error_msg');
		} 
		if (isset($login_success_msg)) {
			$view->login_success_msg = Session::get('login_success_msg');
		}
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->property_types = $property_types;

		//retrieve properties whose owners are ENABLED
		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}

		$view->property = DB::table('property')->where_in('owner', $enabled_owners)->take(3)->order_by('id', 'desc')->get();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	public function action_forgot_password() {
		$view = View::make('home.forgot-password');
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	public function action_reset_password($GUID) {
		$user = User::where('reset_password', '=', $GUID)->first();
		$view = View::make('home.reset-password');
		$view->user = $user;
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;	
	}

	/*
	* this acion is used for email subscription
	*/
	public function action_subscription() {
		$view = View::make('home.subscription');
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action is used to delete a property image
	*/
	public function action_delete_property_image(){
		$image_name = Input::get('img_name');
		$property_id = Input::get('property_id');
		$property = Property::where_id($property_id)->first();
		$image_directory = 'public/uploads/'.sha1($property->owner)."/".$property_id; 
		$path = $image_directory."/".$image_name;
		Log::write('info', "File being deleted >>> ".$path);
		if (File::delete($path)){
			return "image deleted";
		}
		return "image NOT deleted";
	}

	/**
	* this profile action is used to display the profile of a member or agent
	*/
	public function action_profile(){
		$view = View::make('home.profile');

		$view->username = Auth::user()->email;
		$view->usertype = Auth::user()->usertype;
		$view->enabled = Auth::user()->enabled;


		$results = Broker::where_email(Auth::user()->email)->first();
		if (count($results) == 0) {
			$view->details = Member::where_email(Auth::user()->email)->first();
			$view->broker = "false";
		} else {
			$view->details = Broker::where_email(Auth::user()->email)->first();
			$view->broker = "true";
		}
		

		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/*
	this advert ajax action is used to enable/disable adverts
	*/
	public function action_advert_ajax(){
		$id = Input::get('id');
		$advert = Advert::find($id);
		if ($advert->enabled == 1) {
			$return_text = "Disabled";
			$advert->enabled = 0;
		} else {
			$return_text = "Enabled";
			$advert->enabled = 1;
		}
		$advert->save();
		return $return_text;
	}

	/* TODO: delete this action if functionality of sending invoice email works
	* this invoice email action is used to test the sending of invoice emails
	*/
	public function action_invoice_email()
	{
		$view = View::make('email.invoice');
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->property_types = $property_types;
		$view->invoice_email = "true";
		$view->invoice_no = 908978;
		$view->date_generated = "2013-12-12";
		$view->date_due = "2013-12-12";
		$view->client_name = "john doe";
		$view->status = "NOT PAID";
		$view->package = "Gold";
		$view->amount = "6500";
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'Cc: eva@thewebtekies.com' . "\r\n";
		$headers .= 'Cc: peter@kyhillresources.com' . "\r\n";
		$headers .= 'From: info@kyhillresources.com' . "\r\n";
		if (mail('ann@thewebtekies.com', 'Invoice details', $view, $headers)){
			Log::write('info', 'Invoice Email sent');
			return $view;
		}
		Log::write('info', 'Invoice Email not sent');
		return "error occured";
		// return $view;
	}

	/*
	* this send invoice email action is used to send an invoice email to a client who is yet to pay 
	*/
	public function action_send_invoice_email($invoice_no, $date_generated, $date_due, $client_name, $status, $package, $amount, $email)
	{
		$view = View::make('email.invoice');
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->property_types = $property_types;
		$view->invoice_email = "true";
		$view->invoice_no = $invoice_no;
		$view->date_generated = $date_generated;
		$view->date_due = $date_due;
		$view->client_name = $client_name;
		$view->status = $status;
		$view->package = $package;
		$view->amount = $amount;
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: info@kyhillresources.com' . "\r\n";
		if (mail($email, 'Invoice details', $view, $headers)){
			Log::write('info', 'Invoice email sent');
			return "Invoice email sent";
		}
		Log::write('info', 'Invoice Email NOT sent');
		return "Invoice Email NOT sent";
	}

	/**
	* this contactus action is used to render the contact us form
	*/
	public function action_contactus(){
		$view = View::make('home.contactus');
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->property_types = $property_types;
		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}
		$view->property = DB::table('property')->where_in('owner', $enabled_owners)->take(3)->order_by('id', 'desc')->get();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* the users action is a listing of the user in the system. Only an authorized user,
	* in this case an admin, is allowed to view th details
	*/
	public function action_users()
	{
		if (Auth::check()){
			$view = View::make('home.users');
			$view->username = Auth::user()->email;
			$view->usertype = Auth::user()->usertype;
			$view->users = DB::table('users')->paginate(8);
			$property_types = static::get_properties_types();
			$agents = static::get_agents();
			$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
			return $view;
		}
		return Redirect::to('/');
	}

	/**
	* this action allows authorized users, in this case admins, to to view the adverts page
	* which contains a file uploader and a pagination of the images
	*/
	public function action_adverts()
	{
		if (Auth::check()){
			$view = View::make('home.adverts');
			$view->username = Auth::user()->email;
			$view->usertype = Auth::user()->usertype;
			$property_types = static::get_properties_types();
			
			$path = 'public/adverts/';
			$files = scandir($path,1);
			$jpg_images = array();
			$png_images = array();
			$gif_images = array();
			$file_extensions = array('jpg'=>$jpg_images, 'png'=>$png_images, 'gif'=>$gif_images);
			foreach ($files as $file) {
				$file_extension = substr($file, -3);
				if ( in_array($file_extension, array_keys($file_extensions) )){
					array_push($file_extensions[$file_extension], $file);
				}
			}
			$view->jpg_images = $jpg_images;
			$view->png_images = $png_images;
			$view->gif_images = $gif_images;
			$view->advert_images = DB::table('adverts')->paginate(8);

			$agents = static::get_agents();
			$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
			return $view;
		}
		return Redirect::to('/');
	}

	public function action_advert()
	{
		$view = View::make('home.add-advert');
		$view->success_message = Session::get('success_message');
		$property_types = static::get_properties_types();
		$view->property_types = $property_types;
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action is used to upload advert images as the name suggest :-)
	*/
	public function action_upload_advert_image()
	{

		$image = Input::file();
		if (isset($image)){
			$original_name = $image['advert']['name'];

			$advert = new advert;
			$advert->pic_name = $original_name;
			$advert->enabled = 0;
			$advert->save();

			Input::upload('advert', 'public/adverts', $original_name);
			return Redirect::back()->with('advert_save_message', 'advert image saved');
		}
		return Redirect::back()->with('advert_save_message', 'kindly add an image');
	}

	/*
	* this action is used to add a new advert
	*/
	public function action_upload_image()
	{
		$input = Input::all();
		$rules = array(
			'name'  => 'required|max:50',
		    'email' => 'required|email',
		    'advert-link' => 'required|url|unique:adverts,link',
		    'country_code' => 'required',
		    'phone_number' => 'required|integer',
		    'description' => 'required',
		    'advert' => 'required',
		);
		$validation = Validator::make($input, $rules);
		if ($validation->fails())
		{
		    return Redirect::back()->with_errors($validation);
		} 

		$filename = Input::file('advert.name');
		$rst = Advert::where('pic_name', '=', $filename)->get();
		if (!empty($rst)){
			return Redirect::back()->with('unique_error_message', 'The name of the uploaded file already exists. Kindly rename the file');
		}

		$upload_success = Input::upload('advert', 'public/adverts', $filename);
		if ($upload_success) {
			echo "it uploaded";
		} else { echo "failed to upload"; }

		$advert = new advert;
		$advert->pic_name = $filename;
		$advert->enabled = 0;
		$advert->name =	Input::get('name');
		$advert->phone = Input::get('country_code').Input::get('phone_number');
		$advert->email = Input::get('email');	
		$advert->link = Input::get('advert-link');	
		$advert->description = Input::get('description');
		$advert->save();

		return Redirect::back()->with('success_message', 'Your advert submission was a success');
	}

	/**
	* admin_payments action is used to display listings of users pending disabling, users pending approval and
	* users who have been approved. Only admins can view this listings
	*/
	public function action_admin_payments()
	{
		if (Auth::check()){
			$view = View::make('home.admin_payments');
			$view->username = Auth::user()->email;
			$view->usertype = Auth::user()->usertype;
			$view->users = DB::table('users')->paginate(8);
			$view->total_pending_approval = Payment::where_status('PENDING APPROVAL')->count();

			$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
			$enabled_owners = array();
			foreach ($db_enabled_owners as $db_enabled_owner) {
				array_push($enabled_owners, $db_enabled_owner->email);
			}

			// FIX ME: haha this closure doesn't make sense
			$sql = function() {
				return DB::query('
					select 
						users.id as id, 
						memberships.type as usertype,
						memberships.id as membership_id,
						users.enabled as enabled, 
						payments.amount_paid as amount_paid, 
						payments.amount_due as amount_due, 
						payments.next_due_date as next_due_date, 
						payments.owner as owner, 
						payments.membership as membership, 
						payments.status as status, 
						payments.transaction_code as transaction_code, 
						payments.invoice_id as invoice_id
					from 
						payments, users, memberships
					where
						payments.owner=users.email and
						memberships.id=users.usertype and
						users.enabled=1');
			};

			$rs = $sql();


			$rows = array();
			foreach ($rs as $row) {
				$str = strtotime(date("Y-M-d")) - (strtotime($row->next_due_date));
		   		if (floor($str/3600/24) > -7) {
		   			array_push($rows, 
		   						array(
		   							'id' => $row->id,
		   							'usertype' => $row->usertype,
		   							'membership_id' => $row->membership_id,
		   							'invoice_id' => $row->invoice_id, 
		   							'owner' => $row->owner, 
		   							'next_due_date' => $row->next_due_date, 
		   							'days_remaining' => 0-(floor($str/3600/24))
		   						)
		   					);
		   		}
			}	   
			$view->pending_disabling = $rows;
			$view->total_pending_disabling = count($rs);
			$view->payments = DB::table('payments')->get();
			$property_types = static::get_properties_types();
			$agents = static::get_agents();
			$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
			return $view;
		}
		return Redirect::to('/');
	}

	/**
	* this action is used to display a listing of payments (pending payment, pending approval, approved)
	* as well as a from to submit the mpesa transaction code
	*/
	public function action_payments()
	{
		if (Auth::check()){
			$view = View::make('home.payments');
			$view->username = Auth::user()->email;
			$view->usertype = Auth::user()->usertype;
			if (Auth::user()->usertype == 2) {
				$broker = DB::table('brokers')->where_email(Auth::user()->email)->first();
				$membership = DB::table('memberships')->where_id($broker->member_type)->first();
				$view->amount_to_Be_paid = $membership->amount;
			} else if (Auth::user()->usertype == 3) {
				$member = DB::table('members')->where_email(Auth::user()->email)->first();
				$membership = DB::table('memberships')->where_id($member->member_type)->first();
				$view->amount_to_Be_paid = $membership->amount;
			}
			$view->enabled = Auth::user()->enabled;
			$view->payments = DB::table('payments')->where_owner(Auth::user()->email)->paginate(8);
			$not_paid = DB::table('payments')->where_owner_and_status(Auth::user()->email,'NOT PAID')->first();
			if (isset($not_paid)){
				$view->not_paid_invoice_id = $not_paid->invoice_id;
			}
			$property_types = static::get_properties_types();
			$agents = static::get_agents();
			$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
			return $view;
		}
		return Redirect::to('/');
	}

	public function action_update_account() {
		$view = View::make('home.update-account');
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	public function action_reminder_email($user_id){
		$user = User::find($user_id);
		$receiver = $user->email;
		// $receiver = "mark.ekisa@gmail.com";
		$headers = 'From: Kyhillresources Info <info@kyjillresources.com>' . "\r\n";
		$headers .= 'Cc: ann@thewebtekies.com' . "\r\n";
		$headers .= 'Cc: mark.ekisa@gmail.com' . "\r\n";

		$message = "Hi, ". "\r\n";
		$message .= "Your free trial period is nearly over. The good news is you can still". "\r\n";
		$message .= "be a full time user with a membership of either standard, silver or ". "\r\n";
		$message .= "gold". "\r\n". "\r\n";
		$message .= "Thank you". "\r\n". "\r\n";
		$message .= "Kyhill Resources Team". "\r\n". "\r\n";
		if (mail($receiver, "Reminder message", $message, $headers)){
			return Redirect::back()->with('email-notification', 'The reminder email has been sent');
			// return "email sent";
		}
		return "email NOT sent";
	}

	/**
	* this action changes the status of a user either from enabled to disabled or vice versa
	*/
	public function action_change_status()
	{
		Log::write('info','we are changing status' );
		$user_id = Input::get("user-id");
		$user = User::find($user_id);
		$membership_id = $user->usertype;
		$owner = $user->email;
		$new_status = ($user->enabled == 0) ? 1 : 0 ;
		$user->enabled = $new_status;
		$user->save();

		$add_not_paid = Input::get('add_not_paid');
		if ($add_not_paid == "true") {
			$membership = Membership::find($membership_id);

			$date = date_create(date('Y-m-d'));
			date_add($date, date_interval_create_from_date_string('30 days'));

			$amount = $membership->amount;
			$rst = Broker::where_email($email)->first();
			if (!empty($rst)){
				if ($amount != 1000){
					$amount = $amount + 1000;
				}
			}

			$payment = new Payment;
			$payment->amount_due = $amount;
			$payment->next_due_date = date_format($date, 'Y-m-d');
			$payment->owner = $owner;
			$payment->membership = $membership->type;
			$payment->status = "NOT PAID";
			$payment->invoice_id = static::generate_random_number();
			$payment->save();
		}
		return $new_status;
	}

	/**
	* this function generates a random number between 1000000 and 9999999
	*/
	public function generate_random_number(){
		$rst = DB::table('Payments')->get('invoice_id');
		$invoice_ids = array();
		$rnd_num = rand(1000000,9999999);

		foreach ($rst as $rs) {
			array_push($invoice_ids, $rs->invoice_id); 
		}

		while (in_array($rnd_num, $invoice_ids)) {
			$rnd_num = rand(1000000,9999999);
		} 
		
		return $rnd_num;
	}

	/**
	* this is a PHP GUID generator
	*/
	public function GUID(){
	    if (function_exists('com_create_guid') === true){
	        return trim(com_create_guid(), '{}');
	    }
	    return sprintf(
	    				'%04X%04X-%04X-%04X-%04X-%04X%04X%04X', 
				    	mt_rand(0, 65535), 
					    mt_rand(0, 65535), 
					    mt_rand(0, 65535), 
					    mt_rand(16384, 20479), 
					    mt_rand(32768, 49151),
					    mt_rand(0, 65535), 
					    mt_rand(0, 65535), 
					    mt_rand(0, 65535)
				);
	}

	/**
    * this action is used to search for a property based on location, preference, minimum and maximum
    * budget as well as property type
	*/
	public function action_search(){
		$view = View::make('home.search');

		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		$enabled_owners_names = array('admin@administrator.com' => 'Kyhillresources' );
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}

		$rs = DB::table('members')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}
		$rs = DB::table('brokers')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}
		
		$fluent_query = DB::table('property');
		$location = Input::get("location"); 
		$min_price = Input::get("budget_min");
		$max_price = Input::get("budget_max");
		$property_type = Input::get('property_type'); 
		$owner = Input::get('agent'); 

		if (isset($location) && $location != "" ) {
			$fluent_query->where('location', 'LIKE', "%".$location."%");
		} 
		if (isset($property_type) && $property_type != null) {
			$fluent_query->where('property_type', 'LIKE', "%".$property_type."%");
		} 
		if ( (isset($min_price) && $min_price != "") && (isset($max_price) && $max_price != "") ) {
			$fluent_query->where_between('price',$min_price, $max_price);
		}
		if (isset($owner) && $owner != null) {
			$fluent_query->where('owner', 'LIKE', "%".$owner."%");
		}
		$fluent_query->where_in('owner', $enabled_owners);

		$view->search_results = $fluent_query->paginate(9);
		$view->enabled_owners_names = $enabled_owners_names;
		$property_types = static::get_properties_types();
		if (Auth::check()){
			$view->username = Auth::user()->email;
		}
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this method is used to retrieve all the property type
	*/
	public function get_properties_types(){
		$property_types = array();
		$property_types[""] = "--Select a Sub Category--";
		foreach (static::property_data() as $property) {
			foreach ($property as $type) {
				$property_types[$type] = $type;
			}
		}
		return $property_types;
	}

	/**
	* this method is used to retrieved all the registered agents
	*/
	public function get_agents(){
		$agents = array();
		$db_agents = Broker::all();
		$agents[""] = "--Select an Organisation--";
		foreach ($db_agents as $agent) {
			$agents[$agent->email] = $agent->name;
		}
		return $agents;
	}

	/**
	* this method retrieves properties based on categories
	* @param $properties - the properties to be categorized  
	*/
	public function get_property_categories($properties){
		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		$enabled_owners_names = array('admin@administrator.com' => 'Kyhillresources' );
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}

		$rs = DB::table('members')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}
		$rs = DB::table('brokers')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}

		$fluent_query = DB::table('property');
		for($i = 0; $i < count($properties); $i++ ){
			if ($i == 0 || $i == count($properties)){
				$fluent_query->where('property_type', 'LIKE', "%".$properties[$i]."%")->where_in('owner', $enabled_owners);
			} else {
				$fluent_query->or_where('property_type', 'LIKE', "%".$properties[$i]."%")->where_in('owner', $enabled_owners);
			}
		}
		$search_results = $fluent_query->paginate(9);
		return $search_results;
	}

	/**
	* this method retrieves projects by kyhillresourced
	* @param $owner - email address of administrator
	*/
	public function get_projects($owner) {
		$fluent_query = DB::table('property')->where('owner', 'LIKE', "%".$owner."%");
		$search_results = $fluent_query->paginate(9);
		return $search_results;
	}

	public function get_agents_projects($owner) {
		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		$enabled_owners_names = array('admin@administrator.com' => 'Kyhillresources' );
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}

		$rs = DB::table('members')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}
		$rs = DB::table('brokers')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}

		$fluent_query = DB::table('property')->where('owner', 'LIKE', "%".$owner."%")->where_in('owner', $enabled_owners);
		$search_results = $fluent_query->paginate(9);
		return $search_results;	
	}

	public function action_agents(){
		$view = View::make('home.broker-list');
		$rst = DB::table('brokers')->get();
		$agents_emails = array();
		foreach ($rst as $agent) {
			array_push($agents_emails, $agent->email);
		}
		$view->search_results = DB::table('brokers')->paginate(6);
		$view->category = ucfirst("agents' list");
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	public function action_agent_property($agent_id){
		$broker = Broker::find($agent_id);
		$property = Property::where_owner($broker->email)->get();
		return static::get_search_view($broker->email, "agents");
	}


	/**
	* this method returns search view depending on property category
	* @param  $properties - property types
	* @param  $property_category - property categories
	*/
	public function get_search_view($properties, $property_category){
		$view = View::make('home.search');

		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		$enabled_owners_names = array('admin@administrator.com' => 'Kyhillresources' );
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}

		$rs = DB::table('members')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}
		$rs = DB::table('brokers')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}

		$property_types = static::get_properties_types();
		if ($property_category == "Projects") {
			$view->search_results = static::get_projects($properties);
		} else if ($property_category == "agents"){
			$view->search_results = static::get_agents_projects($properties);
		} else {
			$view->search_results = static::get_property_categories($properties);
		}
		$view->category = ucfirst($property_category);
		$view->enabled_owners_names = $enabled_owners_names;
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action returns a view of propeties based on a certain category
	* @param $property_category - category of the properties
	*/
	public function action_property_category($property_category){
		if( $property_category == "residential") {
			$properties = array('house/villa', 'town house', 'apartment');
			return static::get_search_view($properties, $property_category);
		} else if( $property_category == "land") {
			$properties = array('commercial', 'residential', 'agricultural', 'industrial');
			return static::get_search_view($properties, $property_category);
		} else if( $property_category == "commercial") {
			$properties = array('commercial space', 'office', 'builiding/warehouse', 'existing business');
			return static::get_search_view($properties, $property_category);
		} else if( $property_category == "holiday") {
			$properties = array('villa/house', 'apartment', 'town house', 'boutique accomodation', 'room');
			return static::get_search_view($properties, $property_category);
		}
	}

	public function action_categories($id) {
		$view = View::make('home.search');

		$db_enabled_owners = DB::table('users')->where('enabled', '=', 1)->get();
		$enabled_owners = array();
		$enabled_owners_names = array('admin@administrator.com' => 'Kyhillresources' );
		foreach ($db_enabled_owners as $db_enabled_owner) {
			array_push($enabled_owners, $db_enabled_owner->email);
		}

		$rs = DB::table('members')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}
		$rs = DB::table('brokers')->where_in('email', $enabled_owners)->get();
		foreach ($rs as $row) {
			$enabled_owners_names[$row->email] = $row->name;
		}

		$data = DB::query("select 
								* 
						 from 
						 	property 
						 where 
						 	property_type like 
						 		CONCAT('%', (select category from property_categories where id = ?) ,'%') and 
						 	owner in 
						 		(select email from users where enabled = ?)", 
				array($id, '1'));
		$total_portfolios = count($data);
		$total_pages = ceil($total_portfolios / 9);
		$page = Input::get('page', 1);
		if ($page > $total_pages or $page < 1) $page = 1;
		$offset = ($page * 9) - 9;
		$data = array_slice($data, $offset, 9);
		$view->search_results = Paginator::make($data, $total_portfolios, 9);

		$view->category = ucfirst("change this to category");
		$view->enabled_owners_names = $enabled_owners_names;
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action returns the projects view which is managed by the administrator
	*/
	public function action_projects() {
		return static::get_search_view("admin@administrator.com", "Projects");
	}

	/**
	* this action returns a view that is used new property. Only enabled and authenticated users
	* are able to view this view
	*/
	public function action_property()
	{
		//Log::write('info','This is just an informational message' );
		if (Auth::check()){
			if (Auth::user()->enabled == 1 || Auth::user()->usertype ==5 ) {
				$view = View::make('home.property');
				$view->property = static::property_data();
				$username = Auth::user()->email;
				$view->username = $username;
				$property_types = static::get_properties_types();

				$user = User::where_email($username)->first();
				$usertype = $user->usertype;
				$view->usertype = $user->usertype;
				if ($usertype == '1') { 
					static::set_up_authenticated_user($view, 'admin', $username);
				} else if ($usertype == '2') {
					static::set_up_authenticated_user($view, 'brokers', $username);
				} else if ($usertype == '3') {
					static::set_up_authenticated_user($view, 'members', $username);
				} else if ($usertype == '5') {
					$rst = Member::where_email('username')->get();
					if(count($rst)>0) {
						$table = "brokers";
					} else {
						$table = "members";
					}
					static::set_up_authenticated_user($view, $table, $username);
				}		
				$agents = static::get_agents();
				$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
				$view->property_service = static::property_data();
				return $view;
			} else {
				return Redirect::to_action('home@account');		
			}
		}
		return Redirect::to('/');
	}

	/**
	* this method return an associative array of properties that have been categorized
	*/
	public function property_data()
	{
		$categories = [];
		$rst = DB::table('property_categories')->get();
		foreach ($rst as $row) {
			$categories[$row->category] = json_decode($row->descendants, true);
		}
		return $categories;
	}

	/**
	* this action returns a view with a listing of a user's property
	*/
	public function action_account()
	{
		if (Auth::check()){
			$view = View::make('home.account');
			$username = Auth::user()->email;
			$view->username = $username;
			$view->enabled = Auth::user()->enabled; 

			$user = User::where_email($username)->first();
			$usertype = $user->usertype;
			$view->usertype = $usertype;
			$properties = Property::where_owner($username)->paginate(5);
			$property_types = static::get_properties_types();
			$agents = static::get_agents();
			$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
			if ($usertype == '1') { 
				static::set_up_authenticated_user($view, 'admin', $username);
			} else if ($usertype == '2') {
				static::set_up_authenticated_user($view, 'brokers', $username);
			} else if ($usertype == '3') {
				static::set_up_authenticated_user($view, 'members', $username);
			} else if ($usertype == '5') {
				$rst = Member::where_email('username')->get();
				if(count($rst)>0) {
					$table = "brokers";
				} else {
					$table = "members";
				}
				static::set_up_authenticated_user($view, $table, $username);
			}
			return $view;
		}
		return Redirect::to('/');
	}

	/**
	* this action returns the administrator's view
	*/
	public function action_admin()
	{
		$view = View::make("admin");
		$view->username = "administrator";
		return $view;
	}

	/**
	* this method is used by property action to set up an authenticated user
	* setting up - append username and entries to the property view
	* @param $view - the property view
	* @param $table_name - can either broker or member table
	* @param $username - username of the user
	*/
	public function set_up_authenticated_user($view, $table_name, $username)
	{
		if ($username == "admin@administrator.com"){
			$view->membertype = 4;
			$memberships = DB::table('memberships')->where_id(4)->first();
			$view->entries = $memberships->entries;
		} else {
		 	$user_object = DB::table($table_name)->where_email($username)->first();
			$view->membertype = $user_object->member_type;
			$memberships = DB::table('memberships')->where_id($user_object->member_type)->first();
			$view->entries = $memberships->entries;
		}
		$properties = Property::where_owner($username)->paginate(5);
		$view->property_count = Property::where_owner($username)->count(); 
		$view->properties = $properties;
	}

	/**
	* this action returns a view with a detail description of a specific property
	*/
	public function action_detail($id)
	{
		//note: the owner filter is used in restful/post_authenticate hence not required here
		$view = View::make('home.detail');
		if (Auth::check()){
			$view->username = Auth::user()->email;
			$view->enabled = Auth::user()->enabled;
			$view->usertype = Auth::user()->usertype;
		}
		$property = Property::find($id);
		$result = Member::where_email($property->owner)->first();
		if (count($result) > 0) {
			$view->phone_number = $result->phone_number;	
		} else {
			$result = Broker::where_email($property->owner)->first();
			if (count($result) > 0) {
				$view->phone_number = $result->phone_number;	
			} else {
				$view->phone_number = "(+254) 0723321668";	
			}
		}
		$view->property = $property;
		$carousel = array();
		foreach (static::get_saved_images($property->owner, $id) as $image) {
			array_push($carousel, array('image'=>'uploads/'.sha1($property->owner)."/".$id."/".$image,));
		}
		$view->carousel = $carousel;
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action returns a view that allows a user to  edit a property 
	*/
	public function action_property_edit($id)
	{
		$view = View::make('home.property');
		if (Auth::check()){
			$username = Auth::user()->email;
			$view->enabled = Auth::user()->enabled;
			$view->usertype = Auth::user()->usertype;
		}
		$view->property = static::property_data();
		$property = Property::find($id);
		$view->property_edit = $property;
		$view->property_service = static::property_data();
		$view->property_typez = explode("_", $property->property_type);
		$view->saved_images = static::get_saved_images($username, $id);
		$view->username = $username;
		$property_types = static::get_properties_types();

		$user = User::where_email($username)->first();
		$usertype = $user->usertype;

		if ($usertype == '2') {
			$brokers = DB::table('brokers')->where_email($username)->first();
			$view->membertype = $brokers->member_type;
			$memberships = DB::table('memberships')->where_id($brokers->member_type)->first();
			$view->entries = $memberships->entries;
			$view->edit_property = "true";
			$view->property_count = count(Property::where_owner($username)->get());
		} else if ($usertype == '3') {
			$members = DB::table('members')->where_email($username)->first();
			$view->membertype = $members->member_type;
			$memberships = DB::table('memberships')->where_id($members->member_type)->first();
			$view->entries = $memberships->entries;
			$view->edit_property = "true";
			$view->property_count = count(Property::where_owner($username)->get());
		} else if ($usertype == '1') {
			$members = DB::table('users')->where_email($username)->first();
			$view->membertype = 4;
			$memberships = DB::table('memberships')->where_id(4)->first();
			$view->entries = $memberships->entries;
			$view->edit_property = "true";
			$view->property_count = count(Property::where_owner($username)->get());
		}

		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action returns a view that allows a user to delete a property
	*/
	public function action_property_delete($id)
	{
		if (Auth::check()){
			$username = Auth::user()->email;
		}
		$property = Property::find($id);
		$path = 'public/uploads/'.sha1($username)."/".$id;
		File::rmdir($path);
		$property->delete();

		return Redirect::to('home/account');
	}

	/**
	* this action returns a view that allows a new broker/agent to register
	*/
	public function action_broker()
	{
		//$view->users = DB::table('users')->paginate(10);
		$view = View::make('home.broker');
		$login_error_msg = Session::get('login_error_msg');
		$login_success_msg = Session::get('login_success_msg');
		if (isset($login_error_msg)) {
			$view->login_error_msg = $login_error_msg;
		} 
		if (isset($login_success_msg)) {
			$view->login_success_msg = $login_success_msg;
		}
		$property_types = static::get_properties_types();
		$memberships = Membership::where_in('id', array(1, 2, 3, 5))->get();
		$membership_types = array();
		foreach ($memberships as $membership)
		{
			$membership_types[$membership->id] = $membership->type;
		}
		$view->memberships = $membership_types;
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	/**
	* this action returns a view that allows a new member to register
	*/
	public function action_member()
	{
		$view = View::make('home.member');
		$login_error_msg = Session::get('login_error_msg');
		$login_success_msg = Session::get('login_success_msg');
		if (isset($login_error_msg)) {
			$view->login_error_msg = $login_error_msg;
		} 
		if (isset($login_success_msg)) {
			$view->login_success_msg = $login_success_msg;
		}

		$memberships = Membership::where_in('id', array(1,2, 3, 5))->get();
		$membership_types = array();
		foreach ($memberships as $membership)
		{
			$membership_types[$membership->id] = $membership->type;
		}
		$view->memberships = $membership_types;
		$property_types = static::get_properties_types();
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view; 
	}

	/**
	* this method returns an array of saved images of a property based on property id
	* @param $username - the user's username
	* @param $property_id - the id of a property
	*/
	public function get_saved_images($username, $property_id)
	{
		$path = 'public/uploads/'.sha1($username)."/".$property_id;
		$files = scandir($path,1);
		$images = array();
		$file_extensions = array('jpg', 'png', 'gif');
		foreach ($files as $file) {
			$file_extension = substr($file, -3);
			if ( in_array($file_extension, $file_extensions )){
				array_push($images, $file);
			}
		}
		
		return $images;
	}

	/**
	* this action allows a user to logout
	*/
	public function action_logout(){
		Auth::logout();
		return Redirect::to('/');
	}

	/**
	* this action returns a view after a user has attempted to login
	*/
	public function action_login(){
		$view = View::make('home.login');
		$login_error_msg = Session::get('login_error_msg');
		$login_success_msg = Session::get('login_success_msg');
		if (isset($login_error_msg)) {
			$view->login_error_msg = $login_error_msg;
		} 
		if (isset($login_success_msg)) {
			$view->login_success_msg = $login_success_msg;
		}
		$property_types = static::get_properties_types();
		$view->property_types = $property_types;
		$agents = static::get_agents();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;
	}

	public function action_advert_links(){
		$rst = Advert::all();
		$data = array();
		foreach ($rst as $row) {
			$data[$row->pic_name] = $row->link;
		}
		return json_encode($data);
	}

	public function action_getPropertyOfTheMonth(){
		$property = Property::find(Input::get('id'));
		// $property = Property::find(4);
		$data['owner'] = $property->owner;
		$data['id'] = $property->id;
		$data['path'] = URL::to_asset('uploads/'.sha1($property->owner)."/".$property->id."/"); 
		$path = 'public/uploads/'.sha1($property->owner)."/".$property->id."/";
		$files = array_slice(scandir($path, 0), 2);
		$data['files'] = $files;

		return json_encode($data);
	}

	public function action_rate_card(){
		$view = View::make('home.rate-card');
		$agents = static::get_agents();
		$property_types = static::get_properties_types();
		$view->nest('footer', 'home.search_form', array('property_types' => $property_types, 'agents' => $agents));
		return $view;	
	}

	public function action_test() {
		$property_service = [
							'property-agency' => ['buying', 'selling', 'letting'],
							'property-management' => ['commercial', 'residential', 'office space', 'storage'],
							'property-development' => ['building and construction'],
							'property-service-providing' => ['architecure', 'surveying', 'engineering', 'agency', 'managing',
															'interior designing', 'landscaping', 'fabricating', 'woodwork developing'],
							'building-material-supplies' => ['ballast', 'sand', 'building blocks', 'hardware shops', 'roofing', 
															   'plumbing', 'electrical', 'paints', 'steel'],
							'farm-management-companies' => ['farmland', 'estates', 'plantations']
							];
							
		/*foreach ($property_service as $key => $value) {
			DB::table('property_categories')
					->insert(
						array(
							'category' => $key,
							'descendants' => json_encode($value),
						)
					);
		}*/
	}

}