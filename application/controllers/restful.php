<?php 
class Restful_Controller extends Base_Controller {
	public $restful = true;

	/**
	* this restful action handles POST data from member controller action including validation, saving and sending notification email
	*/
	public function post_forgot_password() {
		$to = Input::get("email");

		$input = Input::all();
		$rules = array(
		    'email'  => 'required|email',
		);

		$validation = Validator::make($input, $rules);
		 
		if ($validation->fails())
		{
		    return Redirect::back()->with('errors', $validation->errors->all());
		}

		$user = User::where('email', '=', $to)->first();
		$guid = static::GUID();
		if (count($user) == 1) {
			$user->reset_password = static::GUID();
			$user->save();
		}

		$link = URL::to_action('home@reset_password', array('GUID'=>$guid));
		$headers = 'From: Kyhillresources Info <info@kyjillresources.com>' . "\r\n";
		$headers .= 'From: info@kyhillresources.com' . "\r\n" .

		$message = "Click the following to reset you password:"."\r\n";
		$message .= "$link:"."\r\n";
		if (mail($to, "Reset password", $message, $headers)){
			return Redirect::to_action('home@member')->with('login_success_msg', 'An Email has been sent to the address submitted');
		}
		return "email NOT sent";
	}

	public function post_reset_password() {
		$email = Input::get('email');
		$old_password = Input::get('old-password');
		$new_password = Input::get('new-password');
		$confirm_new_password = Input::get('confirm-new-password');

		$input = Input::all();
		$rules = array(
		    'old-password'  => 'required',
		    'new-password' => 'required|different:old-password',
		    'confirm-new-password' => 'required|same:new-password',
		);

		$validation = Validator::make($input, $rules);
		 
		if ($validation->fails())
		{
		    return Redirect::back()->with('errors', $validation->errors->all());
		}
		
		$user = User::where('email', '=', $email)->first();
		$user->password = Hash::make($new_password);
		$user->reset_password = '';
		$user->save();

		return Redirect::to_action('home@member')->with('login_success_msg', 'You password has been reset');;
	}

	public function post_member()
	{
		$input = Input::all();
		$rules = array(
			'name'  => 'required|max:50',
		    'email' => 'required|email|unique:users',
		    'password' => 'required',
		    'confirm_password' => 'same:password',
		    'country_code' => 'required',
		    'phone_number' => 'required|integer',
		    'member_type' => 'required',
		);
		$validation = Validator::make($input, $rules);
		if ($validation->fails())
		{
		    return Redirect::to('home/member')->with_errors($validation);
		}

		$member = new member;
		$email = Input::get('email');
		$password = Input::get('password');
		$name = Input::get('name');
		$phone_number = Input::get('country_code').Input::get('phone_number');
		$member_type = Input::get('member_type');

		$client_name = $name;
		$member->name = $client_name;
		$member->email = $email;
		$member->phone_number = $phone_number;
		$member->member_type = $member_type;
		$member->save();

		static::send_registration_email($email, $client_name);
		static::send_notification_email($name, $phone_number, $email);
		
		static::create_invoice(Input::get('member_type'), Input::get('email'));
		static::add_member_as_user($email, $password, $member_type);
		return Redirect::to_action('home@member')->with('login_success_msg', 'Congratulations, login to proceed');
	}

	public function post_update_account() {
		$account  = Input::get('account');
		$email  = Input::get('email');

		$user = User::where('email', '=', $email)->first();
		if (empty($user)) {
			return Redirect::back()->with('message', 'user with submitted email does not exist')->with('alert', 'alert-error');
		}
		if ($user->usertype != 5) {
			return Redirect::back()->with('message', 'Currently, this functionality is only for trial account users')->with('alert', 'alert-error');	
		}
		$user->usertype = $account;
		$user->save();

		$membership = Membership::find($account);
		$amount = $membership->amount;
		$rst = Broker::where_email($email)->first();
		if (!empty($rst)){
			if ($amount != 1000){
				$amount = $amount + 1000;
			}
			$rst->member_type = $account;
			$rst->save();
		} else {
			$rst = Member::where_email($email)->first();
			$rst->member_type = $account;
			$rst->save();
		}

		$date = date_create(date('Y-m-d'));
		date_add($date, date_interval_create_from_date_string('7 days'));

		$payment = new Payment;
		$payment->amount_due = $amount;
		$payment->next_due_date = date_format($date, 'Y-m-d');
		$payment->owner = $email;
		$payment->membership = $membership->type;
		$payment->status = "NOT PAID";
		$payment->invoice_id = static::generate_random_number();
		$payment->save();

		return Redirect::back()->with('message', 'update was a success')->with('alert', 'alert-success');
	}

	public function post_advertisers() {
		$input = Input::all();
		$rules = array(
			'name'  => 'required|max:50',
		    'email' => 'required|email',
		    'country_code' => 'required',
		    'phone_number' => 'required|integer',
		);
		$validation = Validator::make($input, $rules);
		if ($validation->fails())
		{
		    return Redirect::back()->with_errors($validation);
		}

		$advertisers = new advertisers;
		$advertisers->name =	Input::get('name');
		$advertisers->phone = Input::get('country_code').Input::get('phone_number');
		$advertisers->email = Input::get('email');	
		$advertisers->enabled = 0;
		$advertisers->save();	

		return Redirect::back()->with('success_message', 'Your have been added as an advertiser');
	}

	/**
	* this restful action handles POST data from payment controller action; users enter MPESA transaction code and 
	* their payment statuses change form 'NOT PAID' to 'PENDING APPROVAL' then the admin can approve the payments
	*/
	public function post_submit_transaction_code(){
		$transaction_code = Input::get("transaction-code");
		$invoice_id = Input::get("invoice-id");
		$payment = Payment::where_invoice_id($invoice_id)->first();
		$payment->status = "PENDING APPROVAL";
		$payment->transaction_code = $transaction_code;
		$payment->save();
		return Redirect::back();
	}

	/**
	* this restful action handles POST data from contactus home controller action and sends an email to the admins
	*/
	public function post_contactus(){
		$input = Input::all();
		$rules = array(
			'client-name'  => 'required|max:50',
		    'phone-number' => 'required',
		    'email' => 'required|email',
		    'email-message' => 'required',
		);

		$messages = array(
		    'client-name_required' => 'Please enter a valid name',
		    'phone-number_required' => 'Please enter a valid phone number',
		    'phone-number_integer' => 'Please enter a valid phone number(numbers)',
		    'email-message_required' => 'Please enter a valid email message',
		);
		$validation = Validator::make($input, $rules, $messages);
		if ($validation->fails())
		{
		    return Redirect::back()->with_errors($validation);
		}

		$message = "Client Name: ".Input::get('client-name')."\r\n";
		$message .= "Phone number: ".Input::get('phone-number')."\r\n";
		$message .= "Email: ".Input::get('email')."\r\n";
		$message .= "Message: ".Input::get('email-message')."\r\n";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'Cc: eva@thewebtekies.com' . "\r\n";
		$headers .= 'Cc: peter@kyhillresources.com' . "\r\n";
		$headers .= 'From: info@kyhillresources.com' . "\r\n";
		if (mail('ann@thewebtekies.com', 'Kyhill Enquiry', $message, $headers)){
			return Redirect::back()->with('success_message', 'Your submission was successful. We shall get back to you as soon as possible');
		}
		return Response::error('404');
	}

	/**
	* this restul action handles POST data from payment controller; once admins approve the payments using this action,
	* the status of a user changes from 'PENDING APPROVAL' to 'APPROVED' and the user is enabled
	*/
	public function post_approve_payment(){
		$invoice_id = Input::get('invoice-id');
		$owner = Input::get('owner');
		$paid_amount = Input::get('paid-amount');

		$payment = Payment::where_invoice_id($invoice_id)->first();
		$payment->amount_paid = $paid_amount;
		$payment->next_due_date = date('Y-m-d', strtotime("+30 days"));
		$payment->status = "APPROVED";
		$payment->save();

		$user = User::where_email($owner)->first();
		$user->enabled = 1;
		$user->save();

		return Redirect::back();
	}

	/**
	* this method is used to add a new member as an authorised member in the system
	* @param $username - the username of the user
	* @param $password - the password of the user
	* @param $usertype - the usertype of the user
	*/
	public function add_member_as_user($username, $password, $usertype)
	{
		$user = new user;
		$user->email = $username;
		$user->password = Hash::make($password);
		$user->usertype = $usertype;
		$user->save();
	}

	/**
	* this method is used to create invoice details of a user whose has just registered or whose monthly
	* subscription has ended. An instance of payment is created based on the user's deails and the status
	* attribute of the payment instance is set to 'NOT PAID'
	* @param $member_type - the member type of the user
	* @param $email - the email of the user
	* @param $list_property - value should either be null or true
	*/
	public function create_invoice($member_type, $email, $list_property = null){
		$membership = Membership::find($member_type);
		$payment = new Payment;
		if ($membership->amount == 1000 || $list_property == null) {
			$amount = $membership->amount;
		} else if ($list_property == "yes"){
			$amount = $membership->amount + 1000;
		}
		
		if ($membership->amount != 0 ) {
			$payment->status = "NOT PAID";
			$invoice_id = static::generate_random_number();
			$payment->invoice_id = $invoice_id;
			$payment->amount_due = $amount; 
		} else {
			$date = date_create(date('Y-m-d'));
			date_add($date, date_interval_create_from_date_string('30 days'));
			$invoice_id = null;
			$payment->status = "TRIAL ACCOUNT";
			$payment->amount_paid = "0";
			$payment->amount_due = "0";
			$payment->next_due_date = date_format($date, 'Y-m-d');
		}
		$payment->owner = $email;
		$payment->membership = $membership->type;
		$payment->save(); 

		$rst = Member::where_email($email)->first();
		if (empty($rst)){
			$rst = Broker::where_email($email)->first();
		}

		$date_generated = date("Y-m-d");
		$date_due = date('Y-m-d', strtotime("+30 days"));
		$client_name = $rst->name;
		$status = "NOT PAID";
		$package = $membership->type;
		$amount = $membership->amount;
		return Controller::call('home@send_invoice_email', array($invoice_id, $date_generated, $date_due, $client_name, $status, $package, $amount, $email));
	}

	/**
	* this function generates a random number between 1000000 and 9999999
	*/
	public function generate_random_number(){

		$rst = DB::table('payments')->get('invoice_id');
		$invoice_ids = array();
		$rnd_num = rand(1000000,9999999);

		foreach ($rst as $rs) {
			array_push($invoice_ids, $rs->invoice_id); 
		}

		while (in_array($rnd_num, $invoice_ids)) {
			$rnd_num = rand(1000000,9999999);
		} 
		
		return $rnd_num;
	}

	/**
	* this method return a gloabal univsersal identifier
	*/
	public function GUID(){
	    if (function_exists('com_create_guid') === true){
	        return trim(com_create_guid(), '{}');
	    }

	    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}

	/**
	* this restful action handles POST data from broker controller action including validation, saving and sending notification email
	*/
	public function post_broker()
	{
		$input = Input::all();
	
		$rules = array(
			'name'  => 'required|max:50|unique:brokers,name',
			'description' => 'required',
		    'email' => 'required|email|unique:users',
		    'password' => 'required',
		    'confirm_password' => 'same:password',
		    'country_code' => 'required',
		    'phone_number' => 'required|integer',
		    'location' => 'required',
		    'representative_name' => 'required',
		    'logo_path.name' => 'unique:brokers,logo_path',
		);

		$validation = Validator::make($input, $rules);
		if ($validation->fails())	
		{
		    return Redirect::to('home/broker')->with_errors($validation);
		}

		$filename = Input::file('logo_path.name');
		if (!empty($filename)) {
			Log::write('info', 'file name >>> '.$filename);
			$upload_success = Input::upload('logo_path', 'public/broker-logos', $filename);
			if ($upload_success) {
				echo "it uploaded";
			} else { echo "failed to upload"; }
		}
		
		$email = Input::get('email');
		$password = Input::get('password');
		$name = Input::get('name');
		$phone_number = Input::get('country_code').Input::get('phone_number');
		$list_property = Input::get('list-property');
		$member_type = Input::get('member_type');

		$broker = new broker;
		$broker_name = Input::get('name');
		$broker->name = $broker_name;
		$broker->description = Input::get('description');
		$broker->email = $email;
		$broker->phone_number = Input::get('country_code').Input::get('phone_number');
		$broker->location = Input::get('location');
		$broker->representative_name = Input::get('representative_name');
		$broker->member_type = $member_type;
		$broker->logo_path = $filename;
		$broker->save();

		static::send_registration_email($email, $broker_name);
		static::send_notification_email($name, $phone_number, $email);
		static::create_invoice(Input::get('member_type'), Input::get('email'), "yes");
		static::add_member_as_user($email, $password, $member_type);
		
		return Redirect::to_action('home@broker')->with('login_success_msg', 'Congratulations, login to proceed');
	}

	/**
	* this restful action handles POST data from property controller action including validation, saving and sending notification email
	*/
	public function post_property()
	{
		/**TODO:validate that a property has atleast 1 image*/
		$inputs = Input::all();
		$rules = array(
			    'description'  => 'required',
			    'price' => 'required|numeric',
			    'location'  => 'required',
			    'action'  => 'required',
			    'name'  => 'required|unique:property,name',
			);

		$id = Input::get('id');
		$ignore_id_rule = 'required|unique:property,name,'.$id;
		if ($id) {
			array_pop($rules);
			$rules['name'] = $ignore_id_rule; 
		}

		$validation = Validator::make($inputs, $rules);
 
		if ($validation->fails()) {
			return Redirect::back()->with_errors($validation);
		}

		/**SESSION OBJECT*/
		if (Auth::check()){
			$username = Auth::user()->email;
		}
		$property_name = Input::get('name');
		$property_type = Input::get('types')."_".Input::get('action');
		$description = Input::get('description');
		$price = Input::get('price');
		$location = Input::get('location');

		/**PROPERTY STUFF*/
		$id = Input::get('id');
		if($id)
			$property = property::find($id);
		else
			$property = new property;
		$property->name = $property_name;  
		$property->description = $description;  
		$property->price = $price;  
		$property->property_type = $property_type;  
		$property->location = $location;
		$property->owner = $username;
		$property->save();

		/**UPLOAD PROPERTY IMAGES*/
		$property = Property::where_name_and_owner($property_name, $username)->first();
		if ($property){
			$image_directory = 'public/uploads/'.sha1($property->owner)."/".$property->id; 
			if (File::mkdir($image_directory)){
				foreach((array) Request::foundation()->files->get('image') as $file) {
					if ($file){
				    	$file->move($image_directory, $file->getClientOriginalName());
					}
				}
			} else { Log::info("directory not created"); }
		} else { Log::info("there is no record"); }

		return Redirect::to('home/account');
	}

	/**
	* this restful action is used to demonstrate a single image file upload
	*/
	public function post_process_upload()
	{
		 $filename = Input::file('image.name');
		 $upload_success = Input::upload('image', 'public/uploads',$filename);
		 if ($upload_success) {
		 	echo "it uploaded";
		 } else { echo "failed to upload"; }
	}

	/**
	* this restful action is used to authenticate a user who wants to login in to the system
	*/
	public function post_authenticate()
	{
		
		$input = Input::all();
		$rules = array(
		    'username' => 'required|email',
		    'signup-password' => 'required',
		);
	
		$which_form = Input::get('which-form');
		$validation = Validator::make($input, $rules);
		if ($validation->fails())
		{
			if ($which_form == "broker-form"){
		    	return Redirect::to_action('home@broker')->with_errors($validation);
		    } else if ($which_form == "member-form"){
		    	return Redirect::to_action('home@member')->with_errors($validation);
			} 
		}

		$username = Input::get('username');
		$password = Input::get('signup-password');
		$credentials = array (
			'username' => $username,
			'password' => $password
		);

		// Log::write('info',"i am going to be authenticated");
		if(Auth::attempt($credentials)) {
			return Redirect::to_action('home@account');
		} else {
			if ($which_form == "broker-form"){
				return Redirect::to_action('home@broker')->with('login_error_msg', 'invalid username or password');
		    } else if ($which_form == "member-form"){
		    	return Redirect::to_action('home@member')->with('login_error_msg', 'invalid username or password');
			}
		}
	}

	/**
	* this restful action is used to send an email to the emails when a prospective client registers
	*/
	public function post_send_email(){
		$to = Input::get("reciever");
		$sender = Input::get("sender");
		$message = Input::get("message");
		$propective_client_name = Input::get("propective-client-name");
		$phone = Input::get('phone');
		$headers = 'From: Kyhillresources Info <info@kyjillresources.com>' . "\r\n";
		$headers .= 'Cc: eva@thewebtekies.com' . "\r\n";
		$headers .= 'Cc: peter@kyhillresources.com' . "\r\n";
		$headers .= 'Cc: ann@thewebtekies.com' . "\r\n";
		$headers .= 'From: info@kyhillresources.com' . "\r\n" .

		$message = "Phone Number:".$phone."\r\n";
		$message .= "Sender:".$sender."\r\n";
		$message .= "Prospective Client Name:".$propective_client_name."\r\n";
		$message .= "Message:".$message."\r\n";
		if (mail($to, "client-agent-message", $message, $headers)){
			return "email sent";
		}
		return "email NOT sent";
	}

	/**
	* this method is used to send an email to a a registered user
	* @param $reciever - the recipient of the message
	* @param $prospective_client_name - the name of the email recipient
	*/
	public function send_registration_email($receiver, $propective_client_name){
		$to = $receiver;
		$headers = 'From: Kyhillresources Info <info@kyjillresources.com>' . "\r\n";

		$message = "Dear $propective_client_name"."\r\n";
		$message .= "Your registration on Kyhill Resources was successful. Kindly pay"."\r\n";
		$message .= "outstanding amount indicated on the invoice attached above to be "."\r\n";
		$message .= "able to list your property with us"."\r\n"."\r\n";
		$message .= "Thank you"."\r\n"."\r\n";
		$message .= "Kyhill Resources Team"."\r\n"."\r\n";
		if (mail($to, "Welcome message", $message, $headers)){
			return "email sent";
		}
		return "email NOT sent";
	}

	/**
	* this method is used to send a notification email to the admins
	*/
	public function send_notification_email($name, $phone_number, $email){
		$message = "Hey Admins, there is a new registration in the system"."\r\n";
		$message .= "Name: ".$name."\r\n";
		$message .= "Phone number: ".$phone_number."\r\n";
		$message .= "Email: ".$email."\r\n";

		$to = 'ann@thewebtekies.com';
		$headers = 'Cc: eva@thewebtekies.com' . "\r\n";
		$headers = 'Cc: peter@kyhillresources.com' . "\r\n";
		$headers .= 'From: info@kyhillresources.com' . "\r\n";

		if (mail($to, "Registration notification", $message, $headers)){
			return "email sent";
		}
		return "email NOT sent";
	}

	/**
	* this method is used retrieved the properties in the database
	*/
	public function get_properties(){
		$property_types = array();
		$property_types[""] = "--Select a property type--";
		foreach (static::property_data() as $property) {
			foreach ($property as $type) {
				$property_types[$type] = $type;
			}
		}
		return $property_types;
	}

	/**
	* this method returns an associative array of properties and their categories
	*/
	public function property_data()
	{
		return array(
				'residential' => array('house/villa', 'town house', 'apartment'),
				'land' => array('commercial', 'residential', 'agricultural', 'industrial'),
				'commercial and other' => array('commercial space', 'office', 'builiding/warehouse', 'existing business'),
				'holiday' => array('villa/house', 'apartment', 'town house', 'boutique accomodation', 'room') 
			);
	}
}