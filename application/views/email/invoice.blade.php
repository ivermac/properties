
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Properties</title>
	<meta name="viewport" content="width=device-width">
	{{ HTML::style('bundles/bootstrapper/css/bootstrap.min.css'); }}
	<style type="text/css">
		
		#search-modal {
			width: 640px;
		}
		.nav-menu {
			padding-top: 35px;
			background-color: #d7ad41;
		}
		.nav-pills li a {
			color: #000000;
		}
		.nav-pills>.active>a, .nav-pills>.active>a:hover, .nav-pills>.active>a:focus {
			color: #fff;
			background-color: #000000;
		}
		.alert-user-panel{
			color: #000;
			background-color: #d7ad41;
		}

		#country-code {
			margin-right: 25px;
		}

		#footer {
	    	height: 40px;
	        padding-top: 20px;
	        background-color: #f5f5f5;
	    }

	    .helper-label-member {
	    	padding-left: 20px;
	    	padding-right: 20px;
	    	margin-left: 180px;
	    }

	    .helper-label-broker {
	    	padding-left: 20px;
	    	padding-right: 20px;
	    }

	    .alert {
	      padding: 8px 35px 8px 14px;
	      margin-bottom: 20px;
	      text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	      background-color: #fcf8e3;
	      border: 1px solid #fbeed5;
	      -webkit-border-radius: 4px;
	         -moz-border-radius: 4px;
	              border-radius: 4px;
	    }

	    .alert-no-result {
	    	color: #c09853;
	    }

	</style>
</head>
<body>
   
	<div class="row" style="margin-left: 32%; width:50%; padding-bottom:5px; margin-bottom: 15px; padding-left: 22px;">
		{{ HTML::image('img/property_logo.jpg', "Kyhillresources", array('class'=>'img-polaroid')) }}
	</div>
	
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 85px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  <h1>INVOICE DETAILS</h1>
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  Invoice No: {{ $invoice_no }}
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  Date generated: {{ $date_generated }}
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  Date due: {{ $date_due }}
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  To: {{ $client_name }}
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  Status: {{ $status }}
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  Package Description: {{ $package }}
		</div>
	</div>
	<div style="width:50%; margin-left: 27%;">
		<div style="
		padding: 8px 35px 8px 14px;
		margin-bottom: 20px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		background-color: #fcf8e3;
		border: 1px solid #fbeed5;
		-webkit-border-radius: 4px;
		   -moz-border-radius: 4px;
		        border-radius: 4px;
		        color: #c09853;
		">
		  Total (KES): {{ $amount }}
		</div>
	
</body>
</html>