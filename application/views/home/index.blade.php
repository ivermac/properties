@layout('master')
@section('body-container')
<style type="text/css">
	.form-signin {
	  padding: 19px 29px 29px;
	  margin: 0 auto 20px;
	}
	.form-signin .form-signin-heading,
	.form-signin .checkbox {
	  margin-bottom: 10px;
	}
	.form-signin input[type="text"],
	.form-signin input[type="password"] {
	  
	  height: auto;
	  margin-bottom: 15px;
	  padding: 7px 9px;
	}
	.social-media{
		margin-left: 29px;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="span8">
			<div class="row">
				<div id="myCarousel" class="carousel slide">
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    <div class="active item">
				    	{{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('img/image1-001.jpg')))); }}
				    </div>
				    <div class="item">
				    	{{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('img/image2-001.jpg')))); }}
				    </div>
				    <div class="item">
				    	{{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('img/image3-001.jpg')))); }}
				    </div>
				  </div>
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			<div class="row">
				<div class="div-container">
					<div class="div-heading">
						<h4>Properties</h4>
					</div>
					<div class="div-body">
						<div class="span4">
							<h4>Property of the month</h4>
						    {{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('adverts/121020_1374492536.jpg')))); }}
						</div>
						<div class="span4">
							<h4>Featured property</h4>
						    {{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('adverts/121020_1374492536.jpg')))); }}
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				@render('top-agents')
			</div>
			<div class="row">
				<hr>
			  	<div class="row-fluid">
			  		<div class="div-container">
			  			<div class="div-heading">
			  				<h4>Latest properties</h4>
			  			</div>
			  			<div class="div-body">
							<ul class="thumbnails">
								@forelse ($property as $prop)
								    <?php 
								    $path = 'public/uploads/'.sha1($prop->owner)."/".$prop->id;
								    $files = scandir($path,1);
								    ?>
									<li class="span4">
										<div class="thumbnail alert-user-panel">
											{{-- HTML::image(URL::to_asset('/uploads/'.sha1($prop->owner)."/".$prop->id."/".$files[0]), '', array('class'=>'img-rounded')); --}}
											@if (strlen(File::extension(URL::to_asset('/uploads/'.sha1($prop->owner)."/".$prop->id."/".$files[0]))) > 0) 
											{{ HTML::image('/uploads/'.sha1($prop->owner)."/".$prop->id."/".$files[0], '...', array('class'=>'img-rounded')); }}
											@else
											{{ HTML::image('/uploads/notfound.png', '...', array('class'=>'img-rounded')) }}
											@endif
											<h3>{{ $prop->name }}</h3>
											<p>{{ (strlen($prop->description) > 30) ? substr($prop->description, 0, 40)."..." : $prop->description }}</p>
											<p>{{ HTML::link('home/detail/'.$prop->id, 'Detail', array('class'=>'btn btn-inverse')); }}</p>
										</div>
									</li>
								@empty
									<div class="alert alert-no-result">
								    	There are no properties :-(
									</div>
								@endforelse
							</ul>
			  			</div>
			  		</div>
					
			  	</div>
			</div>
			<hr>
			<div class="row">
				<div class="div-container">
					<div class="div-heading">
						<h4>More Adverts</h4>
					</div>
					<div class="div-body">
						<div class="span4">
						    {{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('adverts/121020_1374492536.jpg')))); }}
						</div>
						<div class="span4">
						    {{ HTML::decode(HTML::link('/', HTML::image(URL::to_asset('adverts/121020_1374492536.jpg')))); }}
						</div>
					</div>
				</div>
			</div>
		</div>
  		<div class="span3" id="adverts" >
  			@render('adverts')
	    </div>
	</div>

</div>
  	<footer>
  		{{ $footer }} <!--retrived from all controller actions that require it-->
  	</footer>
@endsection

@section('custom-script')
<script type="text/javascript">
	$(function(){
		/*var 
		url = "{{ URL::to('home/getPropertyOfTheMonth') }}",
		id = 5, callback, images = [], path;
		console.log(url);
		$.ajax({
			type: "POST",
            url: url,
            dataType: 'json',
            data: {
                id: id,
            },
            beforeSend: function(){
                
            },
            success: function(data) {
                console.log("success >>> ", data);
                images = [];
                path = data.path;
                
                $.each(data.files, function(key, value){
                	images.push(value);
                })  
                console.log("images >>> ",images);
            	console.log("path >>> ",path);
                var count = 0;
                setInterval(function(){
                	if (count === images.length) {
                		count = 0;
                	}
        			append_advert(path, images, count);
        			count++;
        		},5000);
            },
            error: function(xhr, text, error) {
                console.log("xhr >>> ",xhr);
                console.log("text >>> ",text);
                console.log("error >>> ",error);
            },
		});*/


		function append_advert(dir, image_array, count){
			var 
			path = dir+image_array[count],
			img = $("<img>", { src: path, class:"img-of-the-day" });
            console.log("path >>> ",dir);
			$("#property-of-the-month").empty().append(img);
		}

	});
</script>
@endsection
	

