@layout('master')
@section('body-container')
<div class="alert pull-right alert-user-panel ">
	user email: <span class="label label-info">{{ $username }}</span> 
	user type: <span class="label label-info">
		@if (Auth::user()->usertype == 1)
			{{ 'Administrator' }}
		@elseif (Auth::user()->usertype == 2)
			{{ 'Agent' }}
		@elseif (Auth::user()->usertype == 3)
			{{ 'Member' }}
		@elseif (Auth::user()->usertype == 5)
			{{ 'Free Trial' }}
		@endif 		
	</span> 
	{{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}
</div>
<ul class="nav nav-tabs">
  <li class="active">{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
 @if ($usertype == 1)
 <li >{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
 <li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
 <li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
 <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
 @elseif ($usertype == 2 || $usertype == 3) 
 <li class="{{ ($enabled == 0) ? 'disabled' : '' }}">{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
 <li >{{ HTML::link('home/payments', 'Payments', array('id'=>'payments-link')) }}</li>
 <li >{{ HTML::link('home/profile', 'Profile', array('id'=>'profile-link')) }}</li>
 @else
 <li >{{ HTML::link('home/profile', 'Profile', array('id'=>'profile-link')) }}</li>
 @endif
  <li class="disabled" ><a href="#">Detail View</a></li>
</ul>
@if (count($properties->results)>0)
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>name</td>
			<td>description</td>
			<td>price</td>
			<td>property type</td>
			<td>location</td>
			<td></td>
		</tr>
	</thead>
	<tbody>
		@forelse ($properties->results as $property)
			<tr>
				<td>{{ $property->name }}</td>
				<td>{{ $property->description }}</td>
				<td>{{ $property->price }}</td>
				<?php 
					$splitter = explode("_", $property->property_type);
				?>
				<td>
					@if (isset($splitter[0]))<span class="label label-success">{{ $splitter[0] }}</span> @endif
					@if (isset($splitter[1]))<span class="label label-warning">{{ $splitter[1] }}</span> @endif
					@if (isset($splitter[2]))<span class="label label-info">{{ $splitter[2] }}</span> @endif
				</td>
				<td>{{ $property->location }}</td>
				<td>
					<div class="btn-group">
	                <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
	                <ul class="dropdown-menu">
	                    <li>{{ HTML::link('home/detail/'.$property->id, 'Detail'); }}</li>
	                    <li>{{ HTML::link('home/property_delete/'.$property->id, 'Delete'); }}</li>
	                    <li>{{ HTML::link('home/property_edit/'.$property->id, 'Edit'); }}</li>
	                  </ul>
	              </div>
				</td>
			</tr>
		@empty
			<tr><td>it's lonely in here :-(</tr></td>
		@endforelse
	</tbody>
</table>
{{ $properties->links(); }} 
@else
it's lonely here :-(
@endif
@endsection