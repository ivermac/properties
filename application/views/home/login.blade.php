@layout('master')
@section('body-container')
<style type="text/css">
  body {
    padding-bottom: 40px;
    background-color: #ffffff;
  }

  .form-signin {
    max-width: 300px;
    padding: 19px 29px 29px;
    margin: 0 auto 20px;
    background-color: #fff;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 5px;
       -moz-border-radius: 5px;
            border-radius: 5px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox {
    margin-bottom: 10px;
  }
  .form-signin input[type="text"],
  .form-signin input[type="password"] {
    height: auto;
    margin-bottom: 15px;
    padding: 7px 9px;
  }

</style>
<div class="container">
{{ Form::open('restful/authenticate','POST', array('class'=>'form-signin')) }}
    <h2 class="form-signin-heading">Please sign in</h2>
    @if ( isset($login_error_msg) )
      <p class="alert alert-error">{{ $login_error_msg }}</p>
    @elseif ( isset($login_success_msg) )
      <p class="alert alert-success">{{ $login_success_msg }}</p>
    @endif
    <div class="control-group {{ $errors->has('username') ? 'error' : '' }}">
		<div class="controls">
		  {{ Form::text('username', '', array('placeholder' => 'your email is your username', 'class'=>'input-block-level')); }} 
		  @if ($errors->has('username'))
		  <span class="help-inline">invalid username</span>
		  @endif
		</div>
	</div>

	<div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
		<div class="controls">
		  {{ Form::password('password',array('id' => 'password', 'placeholder'=>'you password comes here', 'class'=>'input-block-level')) }}
		  @if ($errors->has('password'))
		  <span class="help-inline">invalid password</span>
		  @endif
		</div>
	</div>
	{{ Form::submit('login', array('class' => 'btn btn-inverse')) }}
 {{ Form::close() }}
</div> 

<footer>
	{{ $footer }} <!--retrived from all controller actions that require it-->
</footer>

@endsection