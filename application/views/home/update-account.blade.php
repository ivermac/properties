@layout('master')
@section('body-container')
<div class="row">
	<?php $message = Session::get('message'); ?>
	<?php $alert = Session::get('alert'); ?>
	@if (isset($message))
	<div class="alert {{ $alert }} span12">
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	  {{ $message }}
	</div>
	@endif
</div>
<div class="row">
	<div class="span12">
		{{ Form::open_for_files('restful/update_account', 'POST', array('class' => 'form-horizontal')) }}
		{{ Form::text('email', '', array('placeholder' => 'enter your email here')) }}
		<fieldset>
		    <legend>Select your preferred account</legend>
		    <label class="radio">
    			{{ Form::radio('account', '1'); }}
    		  	Standard
    		</label>
    		<label class="radio">
    			{{ Form::radio('account', '2'); }}
    		  	Silver
    		</label>
    		<label class="radio">
    			{{ Form::radio('account', '3'); }}
    		  	Gold
    		</label>
    		{{ Form::submit('submit', array('class' => 'btn btn-primary')) }}
		  </fieldset>
		{{ Form::close() }}

	</div>
</div>
@endsection