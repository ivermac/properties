@layout('master')
@section('body-container')
	<div class="alert pull-right alert-user-panel ">
		Welcome <span class="label label-info">{{ $username }}</span> 
		{{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}
	</div>

	<ul class="nav nav-tabs">
	  	<li >{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))) }}</li>
	  	<li >{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))) }}</li>
		@if ($usertype == 1)
		<li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
		<li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
		<li class="active">{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
		@else
		<li >{{ HTML::link('home/payments', 'Payments', array('id'=>'payments-link')) }}</li>
		@endif
	  	<li class="disabled" ><a href="#">Detail View</a></li>
	</ul>

	<?php $message = Session::get('advert_save_message'); ?>

	@if (isset($message))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			{{ $message }}
		</div>
	@endif

	<div class="row" style="margin-left: 25px;">
		{{-- Form::open_for_files('home/upload_advert_image', 'POST', array('class' => 'form-inline')) --}}
		{{-- Form::file('advert') --}}
		{{-- Form::submit('upload') --}}
		{{-- Form::close() --}}
		<div class="alert alert-warning">Click on the black buttons to enable/disable adverts</div>
	</div>
	<div class="row" style="margin-left: 25px;">
		<ul class="thumbnails">
		<?php $counter = 0; ?>
		@forelse ($advert_images->results as $advert_image)
			@if ($counter%4 == 0 && $counter>0 || $counter == count($advert_images->results))
				<?php $counter = 0; ?>
				</ul>
	  			</div>
			@endif
			@if ($counter == 0 || $counter%4 == 0)
			<div class="row-fluid">
				<ul class="thumbnails">
			@endif
			<li class="span3">
				<div class="thumbnail alert-user-panel">
					{{ HTML::image(URL::to_asset('/adverts/'.$advert_image->pic_name), '', array('class'=>'img-rounded')) }}
					<button class="btn btn-inverse btn-block advert-btn" id="{{ $advert_image->id }}">{{ ($advert_image->enabled == 1) ? "Enabled" : "Disabled" }}</button>
				</div>
			</li>
			<?php $counter++; ?>
		@empty
			<div class="alert alert-no-result">
				No results found :-(
			</div>
		@endforelse
	</div>
	{{ $advert_images->links() }}
@endsection