@layout('master')
@section('body-container')
<div class="alert alert-user-panel pull-right">Welcome <span class="label label-info">{{ $username }} </span> {{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}</div>
<ul class="nav nav-tabs">
  <li>{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
  <li>{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
  @if ($usertype == 1)
  <li class="active">{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
  <li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
  <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
  @else
  <li ><a href="#">Payment</a></li>
  @endif
  <li class="disabled" ><a href="#">Detail View</a></li>
</ul>
<div class="alert alert-info">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Heads up!</strong> Enable or disable a user by clicking on the label. Enabled users have green labels and Disabled ones have red labels
</div>
@if (count($users->results)>0)
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>email</td>
			<td>usertype</td>
			<td>enabled</td>
		</tr>
	</thead>
	<tbody>
		@forelse ($users->results as $user)
			<tr>
				<td>{{ $user->email }}</td>
				<td>
				@if ($user->usertype == 1)
				{{ "Administrator" }}
				@elseif ($user->usertype == 2)
				{{ "Agent" }}
				@elseif ($user->usertype == 3)
				{{ "Member" }}
				@endif
				</td>
				<td>
					{{ ($user->enabled == 0 ) ? Form::button("False", array('class'=>'btn btn-danger change-status', 'data-user-id'=>$user->id)) : Form::button("True", array('class'=>'btn btn-success change-status', 'data-user-id'=>$user->id)) }}
				</td>
			</tr>
		@empty
			<tr><td>it's lonely in here :-(</tr></td>
		@endforelse
	</tbody>
</table>
{{ $users->links(); }} 
@else
it's lonely here :-(
@endif
@endsection