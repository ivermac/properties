@layout('master')
@section('body-container')

<?php 
$success_message = Session::get('success_message');
?>
@if(isset($success_message))
<div class="row">
	<div class="span8 offset2">
		<div class="alert alert-success">
		  	{{ $success_message }}
		</div>
	</div>
</div>
@else
<div class="row">
	<div class="span8">
		<div class="row">
			@render('top-agents')
		</div>
		<br>
		<div class="row">
			<div class="div-container">
			    <div class="div-heading">
			    	<h4>Conatact us</h4>
			    </div>
				<div class="contact-us tab-pane active" id="contact">
					{{ Form::open('restful/contactus', 'POST', array('class' => 'form-horizontal')) }}
					  <div class="control-group {{ $errors->has('client-name') ? 'error' : '' }}">
					    <label class="control-label" for="inputName">Name</label>
					    <div class="controls">
					      <input type="text" name="client-name" placeholder="your name comes here" class="input-block-level" >
					       @if ($errors->has('client-name'))
		      			  <span class="help-inline">@foreach ($errors->get('client-name') as $error){{ $error }}<br> @endforeach</span>
		      			  @endif
					    </div>
					  </div>
					  <div class="control-group {{ $errors->has('phone-number') ? 'error' : '' }}">
					    <label class="control-label" for="inputPhoneNUmber">Phone Number</label>
					    <div class="controls">
					      <input type="text" name="phone-number" placeholder="your phone number comes here" class="input-block-level" >
					       @if ($errors->has('phone-number'))
		      			  <span class="help-inline">@foreach ($errors->get('phone-number') as $error){{ $error }}<br> @endforeach</span>
		      			  @endif
					    </div>
					  </div>
					  <div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
					    <label class="control-label" for="inputEmail">Email</label>
					    <div class="controls">
					      <input type="text" name="email" placeholder="your email comes here" class="input-block-level" >
					       @if ($errors->has('email'))
		      			  <span class="help-inline">@foreach ($errors->get('email') as $error){{ $error }}<br> @endforeach</span>
		      			  @endif
					    </div>
					  </div>
					  <div class="control-group {{ $errors->has('email-message') ? 'error' : '' }}">
					    <label class="control-label" for="inputMessage">Message</label>
					    <div class="controls">
					      <textarea name="email-message" class="input-block-level" rows="5"></textarea>
					       @if ($errors->has('email-message'))
		      			  <span class="help-inline">@foreach ($errors->get('email-message') as $error){{ $error }}<br> @endforeach</span>
		      			  @endif
					    </div>
					  </div>
					  <div class="control-group">
					    <div class="controls">
							<button name="send-email" class="btn btn-inverse ">send</button>
					    </div>
					  </div>
					{{ Form::close(); }}
					<hr>
					<div class="social-media">
						<div class="span2">
							<a target="blank()" href="https://www.facebook.com/pages/Kyhill-Resources/686417021373639">{{ HTML::image('img/facebook.png',"facebook" ) }}</a>
							{{ HTML::image('img/twitter.png', "Kyhillresources") }}
						</div>
						<div class="span5">
							<address>
							  <strong>Kyhill Resources</strong><br>
							  <strong>Location:</strong>Mountain View Mall- Thika road, 3rd floor<br>
							  <strong>Phone:</strong> (+254) 0723321668<br>
							  <strong>Email:</strong> sales@kyhillresources.com/info@kyhillresources.com<br>
							</address>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span3">
		@render('adverts')		
	</div>
</div>
@endif
@endsection

