@layout('master')
@section('body-container')
<div class="row" style="margin-top:-38px">
	<div class="span3">
		@render('signin')
		<hr>
		<div class="packages">
			<table class="table">
              <thead>
                <tr>
                  <th>Member Type</th>
                  <th>(Ksh) Amount/Month</th>
                </tr>
              </thead>
              <tbody>
              	<tr class="info">
              	  <td>Standard</td>
              	  <td>1000</td>
              	</tr>
                <tr class="success">
                  <td>Silver</td>
                  <td>7000</td>
                </tr>
                <tr class="warning">
                  <td>Gold</td>
                  <td>10500</td>
                </tr>
              </tbody>
            </table>
            <p>NB: selecting the 'List property' will result to an additional cost of Ksh.1000</p>
		</div>
	</div>
	<div class="span8 well well-small">
		<h3 class="form-signin-heading">Sign up</h3>
		<ul class="nav nav-tabs">
		  <li>{{ HTML::link('home/member', 'Individual', array('id' => 'item_link')); }}</li>
		  <li class="active">{{ HTML::link('home/broker', 'Organisation/Service Provider', array('id' => 'item_link')); }}</li>
		</ul>
		<div id="invalid-dimensions-error" class="alert alert-danger">The logo should have a dimension of 140 by 140</div>
		{{ Form::open_for_files('restful/broker', 'POST', array('class' => 'form-vertical')) }}
		<div class="row">
			<div class="span4">
				<div class="control-group {{ $errors->has('logo_path') ? 'error' : '' }}">
					{{ Form::label('logo-path', 'Select logo (140x140 pixels)', array('class' => 'control-label')); }}
					<div class="controls">
						{{ Form::file('logo_path') }}
					  @if ($errors->has('logo_path'))
					  <span class="help-inline">Please add a logo</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
					{{ Form::label('name', 'Name', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('name', '', array('placeholder' => 'Name comes here')); }}
					  @if ($errors->has('name'))
					  <span class="help-inline">Please add a name</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
					{{ Form::label('description', 'Description', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_textarea('description', '', array('cols' => '40', 'rows' => '6')) }}
					  @if ($errors->has('description'))
					  <span class="help-inline">Please add a description</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
					{{ Form::label('email', 'Email', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('email', '', array('placeholder' => 'Email comes here')); }}
					  @if ($errors->has('email'))
					  <span class="help-inline">Please add a valid email address</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
					{{ Form::label('password', 'Password', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_password('password', array('placeholder' => 'Password should be more than 9 characters')); }}
					  @if ($errors->has('password'))
					  <span class="help-inline">Please add a password</span>
					  @endif
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="control-group {{ $errors->has('confirm_password') ? 'error' : '' }}">
					{{ Form::label('confirm_password', 'Confirm Password', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_password('confirm_password', array('placeholder' => 'This password should match the one above')); }}
					  @if ($errors->has('confirm_password'))
					  <span class="help-inline">@foreach ($errors->get('confirm_password') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('phone_number') ? 'error' : '' }} {{ $errors->has('country_code') ? 'error' : '' }}">
					{{ Form::label('phone_number', 'Phone Number', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span1_text('country_code', '', array('placeholder' => 'Code', 'id'=>'country-code')) }} {{ Form::span3_text('phone_number', '', array('placeholder' => 'Phone number should by numerical')); }}
					  @if ($errors->has('phone_number'))
					  <span class="help-inline">@foreach ($errors->get('phone_number') as $error){{ $error }}<br> @endforeach</span>
					  @elseif ($errors->has('country_code'))
					  <span class="help-inline">@foreach ($errors->get('country_code') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
					<span class="help-block label helper-label-broker" >Enter country code on field 1 then phone number on field 2</span>
				</div>
				<div class="control-group {{ $errors->has('location') ? 'error' : '' }}">
					{{ Form::label('location', 'Location', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('location', '', array('placeholder' => 'Location comes here')); }}
					  @if ($errors->has('location'))
					  <span class="help-inline">Please add a location</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('representative_name') ? 'error' : '' }}">
					{{ Form::label('represerntative_name', 'Representative Name', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('representative_name', '', array('placeholder' => 'Representative name comes here')); }}
					  @if ($errors->has('representative_name'))
					  <span class="help-inline">Add a representative name</span>
					  @endif
					</div>
				</div>
				<div class="control-group {{ $errors->has('member_type') ? 'error' : '' }}">
					{{ Form::label('member_type', 'Member type', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_select('member_type', $memberships ) }}
					</div>
				</div>
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				{{ Form::submit('submit', array('class' => 'btn btn-inverse', 'id'=>'broker-sumbit')); }}
			</div>
		</div>
		{{ Form::close(); }}
	</div>
</div>

@endsection
