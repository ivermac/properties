@layout('master')
@section('body-container')

@if(isset($success_message))
<div class="row">
	<div class="span8 offset2">
		<div class="alert alert-success">
		  	{{ $success_message }}
		</div>
	</div>
</div>
@else
<?php $message = Session::get('unique_error_message'); ?>

@if (isset($message))
	<div class="alert alert-danger alert-error">
		<button type="button" class="close" data-dismiss="alert">×</button>
		{{ $message }}
	</div>
@endif
<div class="row">
	<div class="span8">
		<div class="row">
			@render('top-agents')
		</div>
		<br>
		<div class="row">
			<div class="div-container">
			    <div class="div-heading">
			    	<h4>Contact us</h4>
			    </div>
				{{ Form::open_for_files('home/upload_image', 'POST', array('class' => 'form-horizontal')) }}
				<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
					{{ Form::label('name', 'Name', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('name', '', array('placeholder' => 'name comes here')); }}
					  @if ($errors->has('name'))
					  <span class="help-inline">@foreach ($errors->get('name') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
					{{ Form::label('email', 'E-mail Address', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('email', '', array('placeholder' => 'Email address comes here')); }}
					   @if ($errors->has('email'))
					  <span class="help-inline">@foreach ($errors->get('email') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('advert-link') ? 'error' : '' }}">
					{{ Form::label('advert-link', 'Advert link', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('advert-link', '', array('placeholder' => 'Advert link comes here')); }}
					   @if ($errors->has('advert-link'))
					  <span class="help-inline">@foreach ($errors->get('advert-link') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('phone_number') ? 'error' : '' }} {{ $errors->has('country_code') ? 'error' : '' }}">
					{{ Form::label('phone_number', 'Phone Number', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span1_text('country_code', '', array('placeholder' => 'Code', 'id'=>'country-code')) }} {{ Form::span3_text('phone_number', '', array('placeholder' => 'Phone number should by numerical')); }}
					  @if ($errors->has('phone_number'))
					  <span class="help-inline">@foreach ($errors->get('phone_number') as $error){{ $error }}<br> @endforeach</span>
					  @elseif ($errors->has('country_code'))
					  <span class="help-inline">@foreach ($errors->get('country_code') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
					<span class="help-block label helper-label-member" >Enter country code on field 1 then phone number on field 2</span>
				</div>

				<div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
					{{ Form::label('description', 'Description', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_textarea('description', '', array('cols' => '40', 'rows' => '6')) }}
					  @if ($errors->has('description'))
					  <span class="help-inline">Please add a description</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('advert') ? 'error' : '' }}">
					<div class="controls">
						{{ Form::file('advert') }}
					  @if ($errors->has('advert'))
					  <span class="help-inline">Please add an image</span>
					  @endif
					</div>
				</div>

				<div class="control-group ">
					<div class="controls">
					  {{ Form::submit('submit', array('class' => 'btn btn-inverse')); }}
					</div>
				</div>
				
				{{ Form::close(); }}
			</div>
		</div>
	</div>
	<div class="span3" id="adverts">
      	@render('adverts')
    </div>
</div>
@endif
@endsection