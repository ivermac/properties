@layout('master')
@section('body-container')
<div class="alert pull-right alert-user-panel ">
	user email: <span class="label label-info">{{ $username }}</span> 
	user type: <span class="label label-info">
		@if (Auth::user()->usertype == 1)
			{{ 'Administrator' }}
		@elseif (Auth::user()->usertype == 2)
			{{ 'Agent' }}
		@elseif (Auth::user()->usertype == 3)
			{{ 'Member' }}
		@elseif (Auth::user()->usertype == 5)
			{{ 'Free Trial' }}
		@endif 		
	</span> 
	{{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}
</div>
<ul class="nav nav-tabs">
  <li >{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
  <li class="{{ ($enabled == 0) ? 'disabled' : '' }}">{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
 @if ($usertype == 1)
 <li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
 <li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
 <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
 @else
 <li >{{ HTML::link('home/payments', 'Payments', array('id'=>'payments-link')) }}</li>
 <li class="active">{{ HTML::link('home/profile', 'Profile', array('id'=>'profile-link')) }}</li>
 @endif
  <li class="disabled" ><a href="#">Detail View</a></li>
</ul>

<dl class="dl-horizontal">
	@if ($broker == "true")
	  <dt></dt>
	  <dd>{{ HTML::image((empty($details->logo_path)) ? "broker-logos/140x140.gif" : "broker-logos/".$details->logo_path, "...") }}</dd>
	  <dt>Name</dt>
	  <dd>{{ $details->name }}</dd>
	  <dt>Description</dt>
	  <dd>{{ $details->description }}</dd>
	  <dt>Email</dt>
	  <dd>{{ $details->phone_number }}</dd>
	  <dt>Location</dt>
	  <dd>{{ $details->location }}</dd>
	  <dt>Representative name</dt>
	  <dd>{{ $details->representative_name }}</dd>
	@else
	  <dt>Name</dt>
	  <dd>{{ $details->name }}</dd>
	  <dt>Email</dt>
	  <dd>{{ $details->email }}</dd>
	  <dt>Phone Number</dt>
	  <dd>{{ $details->phone_number }}</dd>
	@endif
</dl>
@endsection