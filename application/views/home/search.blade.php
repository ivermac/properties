@layout('master')
@section('body-container')
	<div class="container-fluid">

	  	<div class="row-fluid">
		    <div class="span9">
		    	<div class="row">
		    		@render('top-agents')
		    	</div>
		    	<br>
		    	<div class="row">
		    		<div class="div-container">
		    			<div class="div-heading">
		    				@if (isset($category))
				      	  	<h4>{{ $category }}</h4>
				      	  	@else
				      	  	<h4>Search Results</h4>
				      	  	@endif
		    			</div>
		    			<br>
				      	<div class="search-container">
				      	<?php $counter = 0; ?>
				      	@forelse ($search_results->results as $property)
				      		@if ($counter%3 == 0 && $counter>0 || $counter == count($search_results->results))
				      			<?php $counter = 0; ?>
				      			</ul>
				      	  	</div>
				      		@endif
				      		@if ($counter == 0 || $counter%3 == 0)
				      		<div class="row-fluid">
				      			<ul class="thumbnails">
				      		@endif
				      		<li class="span4">
				      			<div class="thumbnail alert-user-panel">
				      				<?php 
				      				$path = 'public/uploads/'.sha1($property->owner)."/".$property->id;
				      				$files = scandir($path,1);
				      				?>
				      				@if (strlen(File::extension(URL::to_asset('/uploads/'.sha1($property->owner)."/".$property->id."/".$files[0]))) > 0) 
				      				{{ HTML::image('/uploads/'.sha1($property->owner)."/".$property->id."/".$files[0], '...', array('class'=>'img-rounded')); }}
				      				@else
				      				{{ HTML::image('/uploads/notfound.png', '...', array('class'=>'img-rounded')) }}
				      				@endif
				      				<h5>Owner: <span class="label label-inverse">{{ $enabled_owners_names[$property->owner] }}</span></h5>
				      				<h5>Property Name: {{ (strlen($property->name) > 20) ? substr($property->name, 0, 20)."..." : $property->name }}</h5>
				      				<p>{{ (strlen($property->description) > 30) ? substr($property->description, 0, 30)."..." : $property->description }}</p>
				      				<p>{{ HTML::link('home/detail/'.$property->id, 'Detail', array('class'=>'btn btn-inverse')); }}</p>
				      			</div>
				      		</li>
				      		<?php $counter++; ?>
				      	@empty
				      		<div class="alert alert-no-result">
				      			No results found :-(
				      	
				      	@endforelse
				      	</div>
				      {{ $search_results->links(); }}
				      </div>
			      </div>
		      </div>
		    </div>
      		<div class="span3" id="adverts">
    	      	@render('adverts')
    	    </div>
	  	</div>
	</div>
@endsection