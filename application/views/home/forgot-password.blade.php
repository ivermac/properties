@layout('master')
@section('body-container')
<div class="page-header">
	<h4>Forgot Password?</h4>
</div>

<?php 
  $validation_errors = Session::get('errors');
?>

@if (isset($validation_errors))
<div class="alert alert-error">
  @foreach ($validation_errors as $value)
      {{ $value }}<br>
  @endforeach
</div>
@endif

{{ Form::open('restful/forgot_password','POST', array('class'=>'form-horizontal')) }}
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
		{{ Form::text('email', '', array('placeholder'=>'Enter you email', 'class'=>'input-xlarge')) }}
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
		{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}
@endsection