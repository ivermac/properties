@layout('master')
@section('body-container')
<div class="row" style="margin-top:-38px">
	<div class="span3">
		@render('signin')
		<hr>
		<div class="packages">
			<table class="table">
              <thead>
                <tr>
                  <th>Member Type</th>
                  <th>(Ksh) Amount/Month</th>
                </tr>
              </thead>
              <tbody>
              	<tr class="info">
              	  <td>Standard</td>
              	  <td>1000</td>
              	</tr>
                <tr class="success">
                  <td>Silver</td>
                  <td>6000</td>
                </tr>
                <tr class="warning">
                  <td>Gold</td>
                  <td>9500</td>
                </tr>
              </tbody>
            </table>
		</div>
	</div>
	<div class="span8 well well-small">
		<h3 class="form-signin-heading">Sign up</h3>
		<ul class="nav nav-tabs">
		  <li class="active">{{ HTML::link('home/member', 'Individual', array('id' => 'item_link')); }}</li>
		  <li>{{ HTML::link('home/broker', 'Organisation/Service Provider', array('id' => 'item_link')); }}</li>
		</ul>
		<div class="row">
			<div class="span8">
				{{ Form::open('restful/member', 'POST', array('class' => 'form-horizontal')); }}
				<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
					{{ Form::label('name', 'Name', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('name', '', array('placeholder' => 'name comes here')); }}
					  @if ($errors->has('name'))
					  <span class="help-inline">@foreach ($errors->get('name') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
					{{ Form::label('email', 'E-mail Address', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_text('email', '', array('placeholder' => 'Email address comes here')); }}
					   @if ($errors->has('email'))
					  <span class="help-inline">@foreach ($errors->get('email') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
					{{ Form::label('password', 'Password', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_password('password', array('placeholder' => 'Password should have more than 9 characters')); }}
					  @if ($errors->has('password'))
					  <span class="help-inline">@foreach ($errors->get('password') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('confirm_password') ? 'error' : '' }}">
					{{ Form::label('confirm_password', 'Confirm Password', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_password('confirm_password', array('placeholder' => 'This password should match the one above')); }}
					  @if ($errors->has('confirm_password'))
					  <span class="help-inline">@foreach ($errors->get('confirm_password') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
				</div>

				<div class="control-group {{ $errors->has('phone_number') ? 'error' : '' }} {{ $errors->has('country_code') ? 'error' : '' }}">
					{{ Form::label('phone_number', 'Phone Number', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span1_text('country_code', '', array('placeholder' => 'Code', 'id'=>'country-code')) }} {{ Form::span3_text('phone_number', '', array('placeholder' => 'Phone number should by numerical')); }}
					  @if ($errors->has('phone_number'))
					  <span class="help-inline">@foreach ($errors->get('phone_number') as $error){{ $error }}<br> @endforeach</span>
					  @elseif ($errors->has('country_code'))
					  <span class="help-inline">@foreach ($errors->get('country_code') as $error){{ $error }}<br> @endforeach</span>
					  @endif
					</div>
					<span class="help-block label helper-label-member" >Enter country code on field 1 then phone number on field 2</span>
				</div>

				<div class="control-group {{ $errors->has('member_type') ? 'error' : '' }}">
					{{ Form::label('member_type', 'Member type', array('class' => 'control-label')); }}
					<div class="controls">
					  {{ Form::span4_select('member_type', $memberships ) }}
					</div>
				</div>

				<div class="control-group ">
					<div class="controls">
					  {{ Form::submit('submit', array('class' => 'btn btn-inverse')); }}
					</div>
				</div>
				
				{{ Form::close(); }}
			</div>
		</div>
	</div>
</div>

@endsection
