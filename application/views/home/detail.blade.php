@layout('master')
@section('body-container')
@if(isset($username))
<div class="alert alert-user-panel pull-right">
	user email: <span class="label label-info">{{ $username }}</span> 
	user type: <span class="label label-info">
		@if (Auth::user()->usertype == 1)
			{{ 'Administrator' }}
		@elseif (Auth::user()->usertype == 2)
			{{ 'Agent' }}
		@elseif (Auth::user()->usertype == 3)
			{{ 'Member' }}
		@elseif (Auth::user()->usertype == 5)
			{{ 'Free Trial' }}
		@endif 		
	</span> 
	{{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}
</div>
<ul class="nav nav-tabs">
  <li>{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
  <li class="{{ ($enabled == 0) ? 'disabled' : '' }}">{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
  @if ($usertype == 1)
  <li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
  <li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
  <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
  @else
  <li >{{ HTML::link('home/payments', 'Payments', array('id'=>'payments-link')) }}</li>
  <li >{{ HTML::link('home/profile', 'Profile', array('id'=>'profile-link')) }}</li>
  @endif
  <li class="active"><a href="#">Detail View</a></li>
</ul>
@endif
<div class="row">
	<div class="div-container">
	    <div class="div-heading">
	    	<h4>Detail View</h4>
	    </div>
		<div class="span8">
				{{ (count($carousel) == 0) ? Alert::warning("No image(s) for this property :(") :  Carousel::create($carousel) }}
		</div>
		<div class="span4">
			<dl class="dl-horizontal">
			  <dt>Property ID</dt>
			  <dd>{{ $property->id }}</dd>
			  <dt>Property Name</dt>
			  <dd>{{ $property->name }}</dd>
			  <dt>Property Price (Ksh)</dt>
			  <dd>{{ $property->price }}</dd>
			  <dt>Property Location</dt>
			  <dd>{{ $property->location }}</dd>
			  <dt>Property/Service</dt>
			  <dd>
			  <?php
			  	$property_type = explode("_", $property->property_type);
			  ?>
			  {{ $property_type[1] }}</dd>
			  <dt>Owner Phone Number</dt>
			  <dd>{{ $phone_number }}</dd>
			</dl>
			@if(!isset($username))
			{{ HTML::link('#contact-owner-modal', 'Contact Property Owner', array('role'=>'button', 'data-toggle'=>'modal', 'class'=>'btn btn-inverse input-block-level')); }}
			@endif
		</div>
	</div>
</div> 
<div class="row">
	<div class="div-heading">
    	<h4>Property Description</h4>
    </div>
    <div class="span12">
    	{{ $property->description }}
    </div>
</div>

<div id="contact-owner-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Contact Owner</h3>
	</div>
	<div class="modal-body">
	<dl class="dl-vertical">
		<dt>Name</dt>
		<dd><input type="text" name="propective-client-name" id="propective-client-name" placeholder="your name comes here" class="input-block-level" > </dd>
		<dt>Phone Number</dt>
		<dd><input type="text" name="phone-number" id="phone-number" placeholder="your phone number comes here" class="input-block-level" > </dd>
		<dt>Email</dt>
		<dd><input type="text" name="email" id="email" placeholder="your email comes here" class="input-block-level" > </dd>
		<dt>Message</dt>
		<dd><textarea name="email-message"id="email-message" class="input-block-level" rows="5"></textarea></dd>
		<button name="send-email" id="send-email" class="btn btn-inverse input-block-level">send email</button></dt>
		<dd></dd>
	</dl>
	</div>
</div>

@endsection
