@layout('master')
@section('body-container')
	<div class="container-fluid">
	  	<div class="row-fluid">
	  		
		    <div class="span9">
		    	<div class="row">
		    		@render('top-agents')
		    	</div>
		    	<br>
		    	<div class="row">
		    		<div class="div-container">
		    		    <div class="div-heading">
		    		    	@if (isset($category))
    			      	  		<h4>{{ $category }}</h4>
    			      	  	@endif
		    		    </div>
		      	
				      	<div class="search-container">
				      		<div class="accordion" id="accordion2">
				      			<?php 
				      				$count = 0;
				      			?>
						      	@forelse ($search_results->results as $agent)
						      		@if ($count == 0)
						      		<div class="alert alert-no-result">
						      			Click on the titles to expand or collapse
						      		</div>
						      		@endif
						      		<div class="accordion-group">
						      		  <div class="accordion-heading">
						      		    <a class="accordion-toggle label" style="background-color: #d7ad41;" data-toggle="collapse" data-parent="#accordion2" href='{{ "#collapse".$count }}'>
						      		      {{ $agent->name }} 
						      		    </a>
						      		  </div>
						      		  <div id='{{ "collapse".$count }}' class='{{ ($count == 0)? "accordion-body collapse in" : "accordion-body collapse" }}'>
						      		    <div class="accordion-inner">
						      		    	<dl class="dl-horizontal">
						      		    	  <dt></dt>
						      		    	  <dd>{{ HTML::image((empty($agent->logo_path)) ? "broker-logos/140x140.gif" : "broker-logos/".$agent->logo_path, "...") }}</dd>
						      		    	  <dt>Email</dt>
						      		    	  <dd>{{ $agent->email }}</dd>
						      		    	  <dt>Phone Number</dt>
						      		    	  <dd>{{ $agent->phone_number }}</dd>
						      		    	  <dt>Location</dt>
						      		    	  <dd>{{ $agent->location }}</dd>
						      		    	  <dt>Representative name</dt>
						      		    	  <dd>{{ $agent->representative_name }}</dd>
						      		    	  <dt>Description</dt>
						      		    	  <dd>{{ $agent->description }}</dd>
						      		    	  <dt></dt>
						      		    	  <dd>{{ HTML::link('home/agent_property/'.$agent->id, 'View properties', array('id' => 'agent-list', 'class'=>'btn btn-small btn-inverse')) }}</dd>
						      		    	</dl>
						      		    </div>
						      		  </div>
						      		</div>
						      		<?php $count++; ?>
						      	@empty
						      		<div class="alert alert-no-result">
						      			No results found :-(
						      		</div>
						      	@endforelse
				      		</div>
				      	{{ $search_results->links(); }}
				      	</div>
				     </div>
				</div>
		    </div>
      		<div class="span3" id="adverts">
    	      	@render('adverts')
    	    </div>
	  	</div>
	</div>

@endsection