@layout('master')
@section('body-container')

<div class="span8">
	<div class="row">
		@render('top-agents')
	</div>
	<div class="row">
		<div class="span8">
			<h4>LISTING</h4>
			 <p>This can be done individually or through our back office. It requires basic information like business name, contacts, 
			 telephone number, addresses, location and logo. We charge Ksh 1,000/- for a 30 day subscription. 
			 Those who take a full year subscription can pay a discounted rate of Ksh 9,000/- enjoying a 25% discount. This charges are inclusive of VAT.  </p>
		</div>
	</div>
	<div class="row">
		<div class="span8">
			<h4>ADVERTISING</h4>
			<p>Property listing:</p>
<p>Standard:: Ksh 1,000/- per month, inclusive of VAT. This allows you to list and advertise 2 properties for a period of 30 days. It can be renewed to avoid delisting. </p>
<p>Silver:: Ksh 6,000/- per month inclusive of VAT. This allows you to list and advertise up to 15 properties for a period of 30 days. It can be renewed to avoid delisting.</p>
<p>Gold:: Ksh 9,500/- per month inclusive of VAT. This allows you to list and advertise up to 30 properties for a period of 30 days. It can be renewed to avoid delisting.</p>
<p>Organizations and service providers pay an extra Ksh 1000/- under Silver and Gold to appear in our listing.</p>

		</div>
	</div>
	 
	<div class="row">
		<div class="span8">
			<h4>PROPERTY AGENCY::</h4>
<p>5 featured top listings – each at Ksh 10,000/- per month, inclusive of V.A.T</p>
<h6>Side bar positions:</h6>
<p>10 boxes - each Ksh 10,000/- per month, inclusive of V.A.T</p>

		</div>
	</div>
	 
	<div class="row">
		<div class="span8">
			  <h4>PROPERTY MANAGEMENT::</h4>
<p>5 featured top listings – each at Ksh 10,000/- per month, inclusive of V.A.T</p>
<h6>Side bar positions:</h6>
<p>10 boxes - each Ksh 10,000/- per month, inclusive of V.A.T</p>

		</div>
	</div>
	 
	<div class="row">
		<div class="span8">
			<h4>PROPERTY DEVELOPMENT::</h4>
<p>5 featured top listings – each at Ksh 10,000/- per month, inclusive of V.A.T</p>
<h6>Side bar positions:</h6>
<p>10 boxes - each Ksh 10,000/- per month, inclusive of V.A.T</p>

		</div>
	</div>
	 
	<div class="row">
		<div class="span8">
			<h4>PROPERTY SERVICE PROVIDERS::</h4>
<p>5 featured top listings – each at Ksh 10,000/- per month, inclusive of V.A.T</p>
<h6>Side bar positions:</h6>
<p>10 boxes - each Ksh 10,000/- per month, inclusive of V.A.T</p>

		</div>
	</div>

	<div class="row">
		<div class="span8">
			<h4>PROPERTY BUILDING MATERIAL SUPPLIERS::</h4>
<p>5 featured top listings – each at Ksh 10,000/- per month, inclusive of V.A.T</p>
<h6>Side bar positions:</h6>
<p>10 boxes - each Ksh 10,000/- per month, inclusive of V.A.T</p>

		</div>
	</div>

	 
	<div class="row">
		<div class="span8">
			<h4>FARM MANAGEMENT COMPANIES::</h4>
<p>5 featured top listings – each at Ksh 10,000/- per month, inclusive of V.A.T</p>
<h6>Side bar positions:</h6>
<p>10 boxes - each Ksh 10,000/- per month, inclusive of V.A.T</p>
		</div>
	</div>
	 
	<p class="lead">{{ HTML::link('home/advert', 'Advertise with us', array('id' => 'advertise-with-us')); }}</p>
</div>
<div class="span3">
	@render('adverts')
</div>

@endsection