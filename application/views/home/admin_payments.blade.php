@layout('master')
@section('body-container')
<style type="text/css">
	.payment-alerts .span6 {
		width: 518px;
	}
</style>
<div class="alert alert-user-panel pull-right">Welcome <span class="label label-info">{{ $username }} </span> {{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}</div>
<ul class="nav nav-tabs">
  <li>{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
  <li>{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
  @if ($usertype == 1)
  <li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
  <li class="active">{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
  <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
  @else
  <li ><a href="#">Payment</a></li>
  @endif
  <li class="disabled" ><a href="#">Detail View</a></li>
</ul>
<div class="row">
	<?php $message = Session::get('email-notification'); ?>
	@if (isset($message))
	<div class="alert alert-success span11">
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	  <h4>Email Notification</h4>
	  {{ Session::get('email-notification') }}
	</div>
	@endif
</div>
<div class="row payment-alerts">
	<div class="alert alert-error alert-block span6">
	  <h4>Pending Disabling</h4>
	  {{ $total_pending_disabling }} client account(s) should be disabled because of non-payment
	</div>
	<div class="alert alert-block span6">
	  <h4>Pending Approval</h4>
	  {{ $total_pending_approval }} clients awaiting payment approval
	</div>
</div>

<ul class="nav nav-tabs" id="payment">
  <li class="active"><a href="#pending-disabling">Pending Disabling</a></li>
  <li><a href="#pending-approval">Pending Approval</a></li>
  <li><a href="#approved">Approved</a></li>
</ul>
 
<div class="tab-content">
  <div class="tab-pane active" id="pending-disabling">
  	@if (count($payments)>0)
	  <table class="table table-striped table-bordered">
	  	<thead>
	  		<tr>
	  			<td>invoice id</td>
	  			<td>email</td>
	  			<td>usertype</td>
	  			<td>due date</td>
	  			<td>Days to due date</td>
	  			<td>enabled</td>
	  			<td>Reminder Email</td>
	  		</tr>
	  	</thead>
	  	<tbody>
	  		@forelse ($pending_disabling as $payment)
	  			
	  			<tr>
	  				<td>{{ $payment['invoice_id'] }}</td>
	  				<td>{{ $payment['owner'] }}</td>
	  				<td>{{ $payment['usertype'] }}</td>
	  				<td>{{ $payment['next_due_date'] }}</td>
	  				<td>{{ $payment['days_remaining'] }}</td>
	  				<td>{{ Form::button("True", array('class'=>'btn btn-success change-status', 'data-user-id'=>$payment['id'], 'data-refresh-page'=>'true', 'data-add-not-paid'=>'true')) }}
	  					
	  				</td>
	  				<td>{{ ($payment['membership_id'] != 5)?:HTML::link('home/reminder_email/'.$payment['id'], 'Send', array('id' => 'rate-card', 'class'=>'btn btn-info')) }}</td>
	  			</tr>
	  		@empty
	  			<tr><td>it's lonely in here :-)</tr></td>
	  		@endforelse
	  	</tbody>
	  </table>
	  @else
	  it's lonely here :-(
	  @endif
  </div>
  <div class="tab-pane" id="pending-approval">
  	 @if (count($payments)>0)
	  <table class="table table-striped table-bordered">
	  	<thead>
	  		<tr>
	  			<td>invoice id</td>
	  			<td>email</td>
	  			<td>due date</td>
	  			<td>status</td>
	  			<td>transaction code</td>
	  			<td>enabled</td>
	  		</tr>
	  	</thead>
	  	<tbody>
	  		@forelse ($payments as $payment)
	  			@if ($payment->status == "PENDING APPROVAL")
	  			<tr>
	  				<td>{{ $payment->invoice_id }}</td>
	  				<td>{{ $payment->owner }}</td>
	  				<td>{{ $payment->next_due_date }}</td>
	  				<td>{{ $payment->status }}</td>
	  				<td>{{ $payment->transaction_code }}</td>
	  				<td>{{ HTML::link('#approve-modal', 'Approve', array('class'=>'btn btn-inverse btn-small pending-approval', 'data-toggle'=>'modal', 'role'=>'button', 'data-user-id'=>$payment->id, 'data-invoice-id'=>$payment->invoice_id, 'data-owner'=>$payment->owner)) }}</td>
	  			</tr>
	  			@endif
	  		@empty
	  			<tr><td>it's lonely in here :-(</tr></td>
	  		@endforelse
	  	</tbody>
	  </table>
	  @else
	  it's lonely here :-(
	  @endif
  </div>
  <div class="tab-pane" id="approved">
  	 @if (count($payments)>0)
	  <table class="table table-striped table-bordered">
	  	<thead>
	  		<tr>
	  			<td>email</td>
	  			<td>due date</td>
	  			<td>status</td>
	  			
	  		</tr>
	  	</thead>
	  	<tbody>
	  		@forelse ($payments as $payment)
	  			@if ($payment->status == "APPROVED")
	  			<tr>
	  				<td>{{ $payment->owner }}</td>
	  				<td>{{ $payment->next_due_date }}</td>
	  				<td>{{ $payment->status }}</td>
	  			</tr>
	  			@endif
	  		@empty
	  			<tr><td>it's lonely in here :-(</tr></td>
	  		@endforelse
	  	</tbody>
	  </table>
	  @else
	  it's lonely here :-)
	  @endif
  </div>
</div>

<div id="approve-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Approval Modal</h3>
  </div>
  {{ Form::open('restful/approve_payment', 'POST', array('class' => 'form-horizontal')); }}
  <div class="modal-body">
    <dl class="dl-horizontal">
      <dt>Invoice Id</dt>
      <dd><input type="text" name="invoice-id" id="invoice-id" disabled="disabled"></label></dd>
      <dt>Email</dt>
      <dd><input type="text" name="owner" id="owner" disabled="disabled"></label></dd>
      <dt>Payed Amount</dt>
      <dd><input type="text" name="paid-amount" id="paid-amount"></label></dd>
    </dl>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    {{ Form::submit('submit', array('class' => 'btn btn-inverse', 'id'=>'approval-modal-submit')); }}
  </div>
  {{ Form::close(); }}
</div>
@endsection