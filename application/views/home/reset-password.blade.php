@layout('master')
@section('body-container')
<div class="page-header">
  <h4>Reset Password</h4>
</div>
<?php 
  $validation_errors = Session::get('errors');
?>

@if (isset($validation_errors))
<div class="alert alert-error">
  @foreach ($validation_errors as $value)
      {{ $value }}<br>
  @endforeach
</div>
@endif

@if (count($user) == 1)
{{ Form::open('restful/reset_password','POST', array('class'=>'form-horizontal')) }}
  {{ Form::hidden('email', $user->email) }}
  <div class="control-group">
    <label class="control-label" for="inputEmail">Old Password</label>
    <div class="controls">
    {{ Form::password('old-password', array('placeholder'=>'Enter Old Password', 'class'=>'input-xlarge')) }}
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputEmail">New Password</label>
    <div class="controls">
    {{ Form::password('new-password', array('placeholder'=>'Enter New Password', 'class'=>'input-xlarge')) }}
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Confirm New Password</label>
    <div class="controls">
    {{ Form::password('confirm-new-password', array('placeholder'=>'Confirm new password', 'class'=>'input-xlarge')) }}
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}
@else
<div class="alert alert-error">
  <h4>Error!</h4>
    Someting went terribly wrong :-(
</div>
@endif

@endsection