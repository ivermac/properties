<div id="search-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Search Form</h3>
	</div>
	<div class="modal-body">
	{{ Form::open('home/search', 'GET', array('class' => 'form-horizontal')) }}
	  <div class="control-group">
	    {{ Form::label('location', 'Location', array('class' => 'control-label')); }}
	    <div class="controls">
	      {{ Form::span4_text('location', '', array('placeholder' => 'location comes here')); }}
	    </div>
	  </div>
	  <div class="control-group">
	    {{ Form::label('sub-categories', 'Sub Categories', array('class' => 'control-label')); }}
	    <div class="controls">
	      {{ Form::span4_select('property_type', $property_types ) }}
	    </div>
	  </div>
		<div class="control-group">
	    {{ Form::label('agent', 'Organisation', array('class' => 'control-label')); }}
	    <div class="controls">
	      {{ Form::span4_select('agent', $agents ) }}
	    </div>
	  </div>
	  <div class="control-group">
	    {{ Form::label('minimium-budget', 'Minimum Budget', array('class' => 'control-label')); }}
	    <div class="controls">
	      {{ Form::span4_text('budget_min', '', array('placeholder' => 'minumum budget amount comes here')); }}
	    </div>
	  </div>
	  <div class="control-group">
	    {{ Form::label('maximum-budget', 'Maximum Budget', array('class' => 'control-label')); }}
	    <div class="controls">
	      {{ Form::span4_text('budget_max', '', array('placeholder' => 'maximum budget amount comes here')); }}
	    </div>
	  </div>
	  <div class="control-group">
	    <div class="controls">
	      {{ Form::submit('search', array('class'=>'btn btn-inverse')) }} 
	    </div>
	  </div>
		
		{{ Form::close() }}
	</div>
	</div>
