@layout('master')
@section('body-container')
<div class="alert alert-user-panel pull-right">
	user email: <span class="label label-info">{{ $username }}</span> 
	user type: <span class="label label-info">
		@if (Auth::user()->usertype == 1)
			{{ 'Administrator' }}
		@elseif (Auth::user()->usertype == 2)
			{{ 'Agent' }}
		@elseif (Auth::user()->usertype == 3)
			{{ 'Member' }}
		@elseif (Auth::user()->usertype == 5)
			{{ 'Free Trial' }}
		@endif 		
	</span> 
	{{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}
</div>
<!-- @yield('menu') -->
<ul class="nav nav-tabs">
  <li>{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
  <li class="active">{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
  @if ($usertype == 1)
  <li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
  <li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
  <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
  @else
  <li >{{ HTML::link('home/payments', 'Payments', array('id'=>'payments-link')) }}</li>
  <li >{{ HTML::link('home/profile', 'Profile', array('id'=>'profile-link')) }}</li>
  @endif
  <li class="disabled" ><a href="#">Detail View</a></li>
</ul>
	<div class="alert alert-error" id="no-images-error-msg">You should atleast upload a single image for the property you are trying to add/edit</div>
{{ Form::open_for_files('restful/property', 'POST', array('class' => 'form-horizontal')); }}
<div class="row">
	<div class="span6">
		<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
			{{ Form::label('name', 'Name', array('class' => 'control-label')); }}
			<div class="controls">
			  {{ Form::text('name', (isset($property_edit) ? $property_edit->name : ''), array('placeholder' => 'name comes here', 'class' => 'input-large')); }}
			   @if ($errors->has('name'))
			  <span class="help-inline">@foreach ($errors->get('name') as $error){{ $error }}<br> @endforeach</span>
			  @endif
			</div>
		</div>
		<div class="control-group {{ $errors->has('price') ? 'error' : '' }}">
			{{ Form::label('price', 'Price', array('class' => 'control-label')); }}
			<div class="controls">
			  {{ Form::prepend_append(Form::text('price', (isset($property_edit) ? $property_edit->price : ''), array('placeholder' => 'Price comes here')), 'KES', '.00') }}
			   @if ($errors->has('price'))
			  <span class="help-inline">@foreach ($errors->get('price') as $error){{ $error }}<br> @endforeach</span>
			  @endif
			</div>
		</div>
		<div class="control-group {{ $errors->has('location') ? 'error' : '' }}">
			{{ Form::label('location', 'Location', array('class' => 'control-label')); }}
			<div class="controls">
			  {{ Form::text('location', (isset($property_edit) ? $property_edit->location : ''), array('placeholder' => 'Location comes here')); }}
			   @if ($errors->has('location'))
			  <span class="help-inline">@foreach ($errors->get('location') as $error){{ $error }}<br> @endforeach</span>
			  @endif
			</div>
		</div>
		<div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
			  {{ Form::large_textarea('description', (isset($property_edit) ? $property_edit->description : ''), array('cols' => '40', 'rows' => '5', 'id'=>'description')) }}
			   @if ($errors->has('description'))
			  <span class="help-inline">@foreach ($errors->get('description') as $error){{ $error }}<br> @endforeach</span>
			  @endif
		</div>
	</div>
	<div class="span6">
		<div class="control-group {{ $errors->has('action') ? 'error' : '' }}">
			{{ Form::radio( "action", "buying", ( isset($property_typez[2]) && ($property_typez[2] == "buying") ? true : false ) ) }} Buying
			{{ Form::radio( "action", "selling", ( isset($property_typez[2]) && ($property_typez[2] == "selling") ? true : false ) ) }} Selling
			<span class="help-inline">@foreach ($errors->get('action') as $error){{ $error }}<br> @endforeach</span>
		</div>
		<div class="accordion" id="accordion2">
			@foreach ($property_service as $name => $values)
		  	<div class="accordion-group">
		    	<div class="accordion-heading">
		      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#{{ $name }}">
		        		{{ $name }}
		      		</a>
		    	</div>
			    <div id="{{ $name }}" class="accordion-body collapse">
			      	<div class="accordion-inner">
			      	@foreach ($values as $value)
			        	{{ Form::radio( "types", $name."_".$value, ( isset($property_typez) && ($property_typez[1] == $value) ? true : false ) ) }} {{ $value }}<br> 
		  			@endforeach
			      	</div>
			    </div>
		  	</div>
		  	@endforeach
		</div>
	</div>
	
</div>
<!-- Button to trigger images modal -->
<a href="#myModal" role="button" class="btn" data-toggle="modal">Add property images</a>

<!--start Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Select property images</h3>
  </div>
  <div class="modal-body">
  	<div class="alert alert-info">
  		<p><strong>Heads up!</strong> Each image should have a minimum width and height of 725 by 295 pixels respectively and a maximum width and height of 2000 by 1000 pixels respectively</p>
	</div>
	<div class="alert alert-block">
  		<p><strong>Heads up!</strong> Press the 'x' button once you have completed selecting the property image/images</p>
	</div>
	<hr>
	@if (isset($saved_images))
		@for ($i = 0; $i < count($saved_images); $i++) 
			{{ $saved_images[$i] }} <button class="del-prop-img" img-name="{{ $saved_images[$i] }}" property-id="{{ $property_edit->id }}">delete</button> <br>
		@endfor
		@for ($i = 0; $i < 5-count($saved_images); $i++) 
			{{ Form::file('image[]', array('multiple'=>true, 'class'=>'not-added')); }}<br>
		@endfor
	@else
		@for ($i = 0; $i < 5; $i++)
			{{ Form::file('image[]', array('multiple'=>true, 'class'=>'not-added')); }}<br>
		@endfor
	@endif
  </div>
</div>
<!--end Modal-->

{{-- for editing --}}
{{ (isset($property_edit) ? Form::hidden('id', $property_edit->id) : null) }}

@if ($membertype == 3)
	{{ Form::submit('submit', array('class' => 'btn btn-primary')); }}
@elseif (isset($property_count)) 
	@if (($entries - $property_count) == 0)
		@if (isset($property_edit))
			{{ Form::submit('submit', array('class' => 'btn btn-primary')); }}
		@else
			<span class="label label-important">Maximun entry reached. You could delete a property and add a new one</span>
		@endif
	@else
		{{ Form::submit('submit', array('class' => 'btn btn-primary', 'id'=>'property-submit-btn')); }}
	@endif
@endif
{{ Form::close(); }}

@endsection
