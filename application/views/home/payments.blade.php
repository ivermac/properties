@layout('master')
@section('body-container')
@if(isset($username))
<div class="alert alert-user-panel pull-right">
  user email: <span class="label label-info">{{ $username }}</span> 
  user type: <span class="label label-info">
    @if (Auth::user()->usertype == 1)
      {{ 'Administrator' }}
    @elseif (Auth::user()->usertype == 2)
      {{ 'Agent' }}
    @elseif (Auth::user()->usertype == 3)
      {{ 'Member' }}
    @elseif (Auth::user()->usertype == 5)
      {{ 'Free Trial' }}
    @endif    
  </span> 
  {{ HTML::link('home/logout', 'Logout', array('class'=>'btn-small btn-danger')) }}
</div>
<ul class="nav nav-tabs">
  <li >{{ HTML::decode(HTML::link('home/account', '<i class="icon-list"></i>My Property List', array('id' => 'item_link'))); }}</li>
  <li class="{{ ($enabled == 0) ? 'disabled' : '' }}">{{ HTML::decode(HTML::link('home/property', '<i class="icon-plus"></i>Add Property', array('id' => 'item_link'))); }}</li>
  @if ($usertype == 1)
  <li >{{ HTML::link('home/users', 'Users', array('id'=>'users-link')) }}</li>
  <li >{{ HTML::link('home/admin_payments', 'Payments', array('id'=>'admin-payment-link')) }}</li>
  <li >{{ HTML::link('home/adverts', 'Adverts', array('id'=>'adverts-link')) }}</li>
  @else
  <li class="active">{{ HTML::link('home/payments', 'Payments', array('id'=>'payments-link')) }}</li>
  <li >{{ HTML::link('home/profile', 'Profile', array('id'=>'profile-link')) }}</li>
  @endif
  <li class="disabled"><a href="#">Detail View</a></li>
</ul>
@endif
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span3">
      Steps to make your MPESA payment
      <ol>
        <li>Navigate to SIM application on your phone</li>
        <li>Select 'MPESA'</li>
        <li>Select 'Lipa na M-PESA'</li>
        <li>Select 'Pay Bill'</li>
        <li>Select 'Enter business no.'</li>
        <li>Enter 335500</li>
        <li>Enter {{ $amount_to_Be_paid }} (amount to be paid)</li>
        <li>Click 'Send'</li>
        <li>Wait to recieve the Transaction code number from safaricom then enter it below</li>
        <li>Finally, click the 'Submit'</li>
      </ol>
      @if (isset($not_paid_invoice_id))
      {{ Form::open('restful/submit_transaction_code', 'POST', array('class' => 'form-horizontal')); }}
      <div class="input-append">
        <input type="hidden" value="{{ $not_paid_invoice_id }}" name="invoice-id" id="invoice-id"/>
        <input class="span10" id="transaction-code" name="transaction-code" type="text" placeholder="Transaction code">
        <!-- <button class="btn" type="button" id="submit-transaction-code">Submit</button> -->
         {{ Form::submit('submit', array('class' => 'btn btn-inverse', 'id'=>'submit-transaction-code')); }}
      </div>
      {{ Form::close(); }}
      @endif
    </div>
    <div class="span9">
      @if (count($payments->results)>0)
      <table class="table table-striped table-bordered">
      	<thead>
      		<tr>
      			<td>Invoice Id</td>
      			<td>Amount Paid</td>
      			<td>Amount Due</td>
      			<td>Next due date</td>
      			<td>Category</td>
      			<td>Status</td>
      		</tr>
      	</thead>
      	<tbody>
      		@forelse ($payments->results as $payment)
      			<tr>
      				<td>{{ $payment->invoice_id }}</td>
      				<td>{{ $payment->amount_paid }}</td>
      				<td>{{ $payment->amount_due }}</td>
      				<td>{{ $payment->next_due_date }}</td>
      				<td>{{ $payment->membership }}</td>
      				<td>{{ $payment->status }}</td>
      			</tr>
      		@empty
      			<tr><td>it's lonely in here :-(</tr></td>
      		@endforelse
      	</tbody>
      </table>
      {{ $payments->links(); }} 
      @else
      it's lonely here :-)
      @endif
    </div>
  </div>
</div>
@endsection