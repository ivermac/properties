<div class="sign-in-form">
{{ Form::open('restful/authenticate','POST', array('class'=>'form-signin')) }}
    <h3 class="form-signin-heading">Sign in</h3>
    @if ( isset($login_error_msg) )
      <p class="alert alert-error">{{ $login_error_msg }}</p>
    @elseif ( isset($login_success_msg) )
      <p class="alert alert-success">{{ $login_success_msg }}</p>
    @endif
    <div class="control-group {{ $errors->has('username') ? 'error' : '' }}">
		<div class="controls">
		  {{ Form::text('username', '', array('placeholder' => 'your email is your username', 'class'=>'input-block-level')); }} 
		  @if ($errors->has('username'))
		  <span class="help-inline">invalid username</span>
		  @endif
		</div>
	</div>

	<div class="control-group {{ $errors->has('signup-password') ? 'error' : '' }}">
		<div class="controls">
		  {{ Form::password('signup-password',array('id' => 'password', 'placeholder'=>'you password comes here', 'class'=>'input-block-level')) }}
		  @if ($errors->has('signup-password'))
		  <span class="help-inline">invalid password</span>
		  @endif
		</div>
	</div>

	{{ Form::hidden('which-form', 'member-form') }}

	{{ Form::submit('login', array('class' => 'btn btn-inverse')) }}
	{{ HTML::link('home/forgot_password', 'Forgot Password?', array('class' => 'btn btn-primary'))}}
{{ Form::close() }}
</div>