
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Properties</title>
	<meta name="viewport" content="width=device-width">
	{{ Asset::container('bootstrapper')->styles(); }}
	{{ Asset::container('bootstrapper')->scripts(); }}
	{{ HTML::style('css/jquery-te-1.4.0.css') }}
	<style type="text/css">
		
		#search-modal {
			width: 640px;
		}
		.nav-menu {
			padding-top: 35px;
			background-color: #d7ad41;
		}
		.nav-pills li a {
			color: #000000;
		}
		.nav-pills>.active>a, .nav-pills>.active>a:hover, .nav-pills>.active>a:focus {
			color: #fff;
			background-color: #000000;
		}
		.alert-user-panel{
			color: #000;
			background-color: #d7ad41;
		}

		#country-code {
			margin-right: 25px;
		}

		#footer {
	    	height: 40px;
	        padding-top: 20px;
	        background-color: #f5f5f5;
	    }

	    .helper-label-member {
	    	padding-left: 20px;
	    	padding-right: 20px;
	    	margin-left: 180px;
	    }

	    .helper-label-broker {
	    	padding-left: 20px;
	    	padding-right: 20px;
	    }

	    .footer {
	       	height: 60px;
	       	background-color: #f5f5f5;
	        margin-left: -20px;
	        margin-right: -20px;
	        padding-left: 20px;
	        padding-right: 20px;
	    }

	    .credit {
	        margin: 20px 0;
	    }

	    .advert-btn {
	    	margin-top: 5px;
	    }

	    .thumbnail>img {
		    max-width: 243px;
		    max-height: 118px;
	    }

	    .div-heading {
	    	background-color: #d7ad41;
	    	padding: 2px 2px 2px 15px;
	    	margin-bottom: 9px;
	    	border: 1px solid #ddd;
	    	-webkit-border-radius: 4px;
	    	-moz-border-radius: 4px;
	    	border-radius: 4px;
	    }

	    #description {
	    	width: 100%;
	    }

	</style>
</head>
<body>
	
   
	<div class="row" style="background-color: #d7ad41;">
		<div class="container" >
			<div class="row">
				<div class="span3">
					{{ HTML::image('img/property_logo.jpg', "Kyhillresources", array('class'=>'img-polaroid')) }}
				</div>
				@if (!isset($usertype))
				<div class="span9 nav-menu">
					<ul class="nav nav-pills pull-right">
				        <li>{{ HTML::decode(HTML::link('home/', '<i class="icon-home"></i> Home', array('id' => 'home'))); }}</li>
					    <li class="divider-vertical"></li>
					    <li class="divider-vertical"></li>
				      	<li>{{ HTML::decode(HTML::link('#search-modal', '<i class="icon-search"></i> Search', array('role'=>'button', 'data-toggle'=>'modal'))) }}</li>
					    <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-filter" ></i>
							Listing
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li>{{ HTML::link('home/projects', 'Our Projects', array('id' => 'our-projects')); }}</li>
								<li class="nav-header">Categories</li>
								<li>{{ HTML::link('home/categories/1', 'Property Agency', array('id' => 'property-agency')); }}</li>
								<li>{{ HTML::link('home/categories/2', 'Property Management', array('id' => 'propery-management')); }}</li>
								<li>{{ HTML::link('home/categories/3', 'Property Development', array('id' => 'property-development')); }}</li>
								<li>{{ HTML::link('home/categories/4', 'Service Providing', array('id' => 'service-providing')); }}</li>
								<li>{{ HTML::link('home/categories/5', 'Building Material Supplies', array('id' => 'building-material-supplies')); }}</li>
								<li>{{ HTML::link('home/categories/6', 'Farm Management Supplies', array('id' => 'farm-management-supplies')); }}</li>
				        		<li>{{ HTML::decode(HTML::link('home/agents', '<i class="icon-list"></i> Organisations/Service Providers', array('id' => 'agent-list'))); }}</li>
							</ul>	
						</li>
				      	<li class="divider-vertical"></li>
				        <li>{{ HTML::decode(HTML::link('home/member', '<i class="icon-list"></i> List your Property/Service', array('id' => 'registration'))); }}</li>
				      	<li class="divider-vertical"></li>
				        <li>{{ HTML::link('home/advert', 'Advertise with us', array('id' => 'advertise-with-us')); }}</li>
				      	<li class="divider-vertical"></li>
				        <li>{{ HTML::decode(HTML::link('home/rate_card', '<i class="icon-list-alt"></i> Rate Card', array('id' => 'rate-card'))); }}</li>
				      	<li class="divider-vertical"></li>
				        <li>{{ HTML::decode(HTML::link('home/contactus', '<i class="icon-envelope"></i> Contact Us', array('id' => 'contact-us'))); }}</li>
				      	<li class="divider-vertical"></li>
				        <li>{{ HTML::decode(HTML::link('#help-modal', '<i class="icon-question-sign"></i> Help', array('id' => 'help-link', 'data-toggle' => "modal"))); }}</li>
			        </ul>
				</div>
				@endif
			</div>
		</div>
	</div>
	<div class="container" style="padding-top:40px;">
		@yield('body-container')
	</div>
	<footer>
		<div id="help-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Help</h3>
			</div>
			<div class="modal-body">
				<h3>1.    How to list your Property</h3>
				Click on the <code>'List your Property'</code> link on the menu and you will be redirected to a page with 2 sections, <code>SignIn</code> and <code>SignUp</code> sections. If you are a new user, you will want to Signup, hence the <code>Signup</code> section will be the most appropriate for you. The SignUp section has 2 tabs<br>
				<ol>
					<li><code>Member</code></li>
					<li><code>Agent</code></li>
				</ol>
				If you are an individual/company with an agent/broker it is advisable you sign up as an <code>agent</code> but if you want to be the direct link to your prospective client, then signing up as an <code>member</code> is advisable. Correctly fill the form that you have selected then submit the form. If your field values are correct, you will be redirected to the same page but with a <code>'successful submission'</code> message that allows you to sign in into the system.<br>

				if you are an existing user, enter your correct email and password credentials on the <code>sign-in</code> section then submit the form
				 
				<h3>2.       How to make payment</h3>

				After Logging in to your account the first step is to make a payment against your account. Click on the <code>'payment'</code> tab then follow the steps given
				<h3>3.       Listing your property</h3>

				After a successful payment and approval, proceed to list your property.
			</div>
		</div>

		{{ $footer }}
		<div class="container">
			<div class="footer credit "><p class="pull-right">Developed by: {{ HTML::link('http://www.thewebtekies.com/', 'The Web Tekies') }}</p></div>
		</div>
	</footer>
	<section class="scripts">
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-48289095-1', 'kyhillresources.com');
		  ga('send', 'pageview');

		</script>
		
		{{ HTML::script('js/master.js') }}
		{{ HTML::script('js/jquery-te-1.4.0.min.js') }}
		<script type="text/javascript">
			$(function(){
				var urls = {
					adverts_link: "{{ URL::to('home/advert_links') }}",
					get_advert_image_links: "{{ URL::to('home/get_advert_image_links') }}",
					adverts: "{{ URL::to('adverts/') }}",
					home: "{{ URL::to('/') }}",
					advert_ajax: "{{ URL::to('home/advert_ajax/') }}",
					delete_property_image: "{{ URL::to('home/delete_property_image/') }}",
					change_status: "{{ URL::to('home/change_status') }}",
					send_email: "{{ URL::to('restful/send_email') }}"
				};
				master(urls);
			});
		</script>
		@yield('custom-script')
	</section>
</body>
</html>